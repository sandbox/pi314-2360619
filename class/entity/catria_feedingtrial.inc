<?php

/**
 * @file
 * This file contains the CatriaAttribute class.
 */

/**
 * Class for Catria attribute-objects.
 */
class CatriaFeedingtrial extends CatriaEntity {

  const ADDTITLE = 'Add a new feedingtrial';
  const OVERVIEW_PAGE = 'catria/feedingtrials';
  const DEFAULT_URI  = 'catria/feedingtrial/';
  public $userid;

  /**
   * Returns view array for this object.
   *
   * @param string $view_mode
   *   Parameter for parent function.
   * @param string $langcode
   *   Parameter for parent function.
   * @param string $page
   *   Parameter for parent function.
   *
   * @return array
   *   A view array.
   */
  public function view($view_mode = 'full', $langcode = NULL, $page = NULL) {
    $view = parent::view();
    global $user;
    if ($this->getVariables()) {
      $view['variables'] = entity_get_controller('catria_variable')->overviewFeedingtrial($this);
      if ($this->getAnimals()) {
        $view['animals'] = entity_get_controller('catria_animal')->overviewFeedingtrial($this);
        $view['measurements'] = entity_get_controller('catria_measurement')->overviewFeedingtrial($this);
      }
      else {
        if ($user->uid == $this->getUser()->uid) {
          $view['animalfile'] = drupal_get_form('catria_feedingtrial_upload_animalfile_form', $this);
        }
      }
    }
    else {
      if ($user->uid == $this->getUser()->uid) {
        $view['variablefile'] = drupal_get_form('catria_feedingtrial_upload_variablefile_form', $this);
      }
    }
    return $view;
  }

  /**
   * Submits edit form state.
   *
   * @param array $form_state
   *   Form state array.
   */
  public function getEditFormSubmit(array $form_state) {
    parent::getEditFormSubmit($form_state);
    global $user;
    $this->setUser($user);
  }

  /**
   * Returns upload variablefile form.
   *
   * @param array $form
   *   Form array.
   * @param array $form_state
   *   Form state array.
   *
   * @return array
   *   Form array.
   */
  public function getUploadVariablefileForm(array $form, array $form_state) {
    $form['file'] = array(
      '#type' => 'managed_file',
      '#title' => t('Set variables'),
      '#upload_validators' => array('file_validate_extensions' => array('csv')),
      '#upload_location' => 'private://feedingtrial/' . $this->getName() . '/',
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('submit'),
      '#submit' => array('catria_feedingtrial_upload_variablefile_form_submit'),
      '#validate' => array('catria_feedingtrial_upload_variablefile_form_validate'),
    );
    return $form;
  }

  /**
   * Returns upload animalfile form.
   *
   * @param array $form
   *   Form array.
   * @param array $form_state
   *   Form state array.
   *
   * @return array
   *   Form array.
   */
  public function getUploadAnimalfileForm(array $form, array $form_state) {
    $form['animalfile'] = array(
      '#type' => 'managed_file',
      '#title' => t('Set animals'),
      '#upload_validators' => array('file_validate_extensions' => array('csv')),
      '#upload_location' => 'private://feedingtrial/' . $this->getName() . '/',
    );
    $form['animalfilesubmit'] = array(
      '#type' => 'submit',
      '#value' => t('upload animal file'),
      '#submit' => array('catria_feedingtrial_upload_animalfile_form_submit'),
      '#validate' => array('catria_feedingtrial_upload_animalfile_form_validate'),
    );
    return $form;
  }

  /**
   * Validates variablefile.
   *
   * @param object $file
   *   File object.
   *
   * @return bool
   *   A boolean value for validation.
   */
  public function validateVariablefile($file) {
    $valid = TRUE;
    if (isset($file->uri)) {
      $fp = fopen($file->uri, 'r');
      while (($temp_line = fgetcsv($fp, 0, ',')) !== FALSE) {
        if (count($temp_line) != 2) {
          $valid = FALSE;
        }
      }
      return $valid;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Integrates variablefile.
   *
   * @param object $file
   *   File object.
   */
  public function setVariablefile($file) {
    if (isset($file->uri)) {
      $fp = fopen($file->uri, 'r');
      $variable_controller = new CatriaVariableController('catria_variable');
      while (($temp_line = fgetcsv($fp, 0, ',')) !== FALSE) {
        $variable = $variable_controller->create();
        $variable->setName($temp_line[0]);
        $variable->setDescription($temp_line[1]);
        $variable->setFeedingtrial($this);
        $variable->save();
      }
    }
  }

  /**
   * Validates animalfile.
   *
   * @param object $file
   *   File object.
   *
   * @return bool
   *   A boolean value for validation.
   */
  public function validateAnimalfile($file) {
    if (isset($file->uri)) {
      $fp = fopen($file->uri, 'r');
      $columns = fgetcsv($fp, 0, ',');
      $fp = fopen($file->uri, 'r');
      $columns = fgetcsv($fp, 0, ',');
      $colnums = count($columns);
      $variables = $this->getVariables();
      $variabless = array();
      foreach ($variables as $variable) {
        $variabless[$variable->getName()] = $variable;
      }
      for ($i = 2; $i < $colnums; $i++) {
        if (!isset($variabless[$columns[$i]])) {
          return FALSE;
        }
      }
      while (($temp_line = fgetcsv($fp, 0, ',')) !== FALSE) {
        if (count($temp_line) != $colnums) {
          return FALSE;
        }
      }
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Integrates animalfile.
   *
   * @param object $file
   *   File object.
   */
  public function setAnimalfile($file) {
    if (isset($file->uri)) {
      $fp = fopen($file->uri, 'r');
      $columns = fgetcsv($fp, 0, ',');
      $colnums = count($columns);
      $variables = $this->getVariables();
      $variabless = array();
      foreach ($variables as $variable) {
        $variabless[$variable->getName()] = $variable;
      }
      $animal_controller = new CatriaAnimalController('catria_animal');
      $describes_controller = new CatriaDescribesController('catria_describes');
      while (($temp_line = fgetcsv($fp, 0, ',')) !== FALSE) {
        $animal = $animal_controller->create();
        $animal->setName($temp_line[0]);
        $animal->setDescription($temp_line[1]);
        $animal->setFeedingtrial($this);
        $animal->save();
        for ($i = 2; $i < $colnums; $i++) {
          $describes_controller->setDescribesValue($animal, $variabless[$columns[$i]], $temp_line[$i])->save();
        }
      }
    }
  }

  /**
   * Returns variable-objects.
   *
   * @return array|FALSE
   *   Array with objects of type CatriaVariable.
   */
  public function getVariables() {
    if (isset($this->id)) {
      return entity_get_controller('catria_variable')->loadVariablesByFeedingtrial($this);
    }
    return FALSE;
  }

  /**
   * Returns animal-objects.
   *
   * @return array|FALSE
   *   Array with objects of type CatriaAnimal.
   */
  public function getAnimals() {
    if (isset($this->id)) {
      return entity_get_controller('catria_animal')->loadAnimalsByFeedingtrial($this);
    }
    return FALSE;
  }

  /**
   * Returns measurement-objects.
   *
   * @return array|FALSE
   *   Array with objects of type CatriaMeasurement.
   */
  public function getMeasurements() {
    if (isset($this->id)) {
      return entity_get_controller('catria_measurement')->loadMeasurementsByFeedingtrial($this);
    }
    return FALSE;
  }

  /**
   * Deletes variable-objects.
   */
  public function deleteVariables() {
    entity_get_controller('catria_variable')->deleteVariablesByFeedingtrial($this);
  }

  /**
   * Deletes animal-objects.
   */
  public function deleteAnimals() {
    entity_get_controller('catria_animal')->deleteAnimalsByFeedingtrial($this);
  }

  /**
   * Deletes measurement-objects.
   */
  public function deleteMeasurements() {
    if ($measurements = $this->getMeasurements()) {
      foreach ($measurements as $measurement) {
        $measurement->delete();
      }
    }
  }


  /**
   * Deletes object.
   */
  public function delete() {
    if (isset($this->id)) {
      $this->deleteAnimals();
      $this->deleteVariables();
      $this->deleteMeasurements();
      entity_get_controller('catria_feedingtrial')->delete(array($this->id));
    }
  }

  /**
   * Returns statistic view.
   *
   * @return array
   *   Array with statistics view.
   */
  public function statisticsView() {
    $view = array();
    $header = array(
      t('#Variables'),
      t('#Animals'),
      t('#Measurements'),
      t('#Attributes'),
      t('#Measurement values'),
    );
    $rows = array(array(0, 0, 0, 0, 0));
    if ($variables = $this->getVariables()) {
      $rows[0][0] = count($variables);
    }
    if ($animals = $this->getAnimals()) {
      $rows[0][1] = count($animals);
    }
    if ($measurements = $this->getMeasurements()) {
      $rows[0][2] = count($measurements);
      $tmp_attributes = 0;
      $tmp_values = 0;
      foreach ($measurements as $measurement) {
        $attributes = $measurement->getAttributes();
        $tmp_attributes += count($attributes);
        foreach ($attributes as $attribute) {
          $determiness = $attribute->getDeterminess();
          $tmp_values += count($determiness);
        }
      }
      $rows[0][3] = $tmp_attributes;
      $rows[0][4] = $tmp_values;
    }
    $view = array(
      '#type' => 'item',
      '#title' => t('Statistics'),
      '#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
    );
    return $view;
  }

}
