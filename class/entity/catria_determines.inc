<?php

/**
 * @file
 * Contains the CatriaAttribute class.
 */

/**
 * Represents Catria attribute-objects.
 */
class CatriaDetermines extends CatriaEntity {

  public $animalid;
  public $attributeid;
  public $timepoint;
  public $value;

  /**
   * Sets value.
   *
   * @param int $value
   *   An integer with value.
   */
  public function setValue($value) {
    $this->value = $value;
  }

  /**
   * Returns value.
   *
   * @return int|FALSE|XXXX
   *   An integer with value.
   */
  public function getValue() {
    global $user;
    $attribute = $this->getAttribute();
    $measurement = $attribute->getMeasurement();
    if ($measurement->getHasAccess($user) == 'administrator') {
      if (isset($this->value)) {
        return $this->value;
      }
      return FALSE;
    }
    else {
      if ($measurement->getStatus() == 'closed') {
        return 'XXXX';
      }
      if ($measurement->getStatus() == 'restricted') {
        if ($measurement->getHasAccess($user) == 'allowed') {
          if (isset($this->value)) {
            return $this->value;
          }
          return FALSE;
        }
        else {
          return 'XXXX';
        }
      }
      if ($measurement->getStatus() == 'intern') {
        if (user_access('use catria intern', $user)) {
          if (isset($this->value)) {
            return $this->value;
          }
          return FALSE;
        }
        else {
          return 'XXXX';
        }
      }
      if ($measurement->getStatus() == 'open') {
        if (isset($this->value)) {
          return $this->value;
        }
        return FALSE;
      }
    }
  }

  /**
   * Sets timepoint.
   *
   * @param string $timepoint
   *   A string with timepoint.
   */
  public function setTimepoint($timepoint) {
    $this->timepoint = check_plain($timepoint);
  }

  /**
   * Returns timepoint.
   *
   * @return value
   *   Timepoint.
   */
  public function getTimepoint() {
    if (isset($this->timepoint)) {
      return check_plain($this->timepoint);
    }
    return FALSE;
  }

}
