<?php

/**
 * @file
 * Contains the CatriaHas class.
 */

/**
 * Represents Catria has-objects.
 */
class CatriaHas extends CatriaEntity {

  public $userid;
  public $measurementid;
  public $access;

  /**
   * Sets access.
   *
   * @param string $access
   *   A string with access.
   */
  public function setAccess($access) {
    $this->access = check_plain($access);
  }

  /**
   * Returns access.
   *
   * @return string|FALSE
   *   A string with access.
   */
  public function getAccess() {
    if (isset($this->access)) {
      return check_plain($this->access);
    }
    return FALSE;
  }

}
