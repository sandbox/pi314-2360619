<?php

/**
 * @file
 * This file contains the CatriaAttribute class.
 */

/**
 * Class for Catria attribute-objects.
 */
class CatriaAttribute extends CatriaEntity {

  const OVERVIEW_PAGE = 'catria/attribute/overview/';
  const DEFAULT_URI  = 'catria/attribute/';
  public $unit;
  public $target;
  public $measurementid;

  /**
   * Returns view array for this object.
   *
   * @param string $view_mode
   *   Parameter for parent function.
   * @param string $langcode
   *   Parameter for parent function.
   * @param string $page
   *   Parameter for parent function.
   *
   * @return array
   *   View array.
   */
  public function view($view_mode = 'full', $langcode = NULL, $page = NULL) {
    $view = parent::view();
    $view['unit'] = array('#markup' => t('<b>Unit:</b>') . $this->getUnit());
    if ($determiness = $this->getDeterminess()) {
      $header = array(t('Animal'), t('timepoint'), t('value'));
      $rows = array();
      foreach ($determiness as $determines) {
        $animal = $determines->getAnimal();
        $rows[] = array(
          l($animal->getName(), 'catria/animal/' . $animal->getId()),
          $determines->getTimepoint(), $determines->getValue(),
        );
      }
      $view['animals'] = array(
        '#type' => 'item',
        '#title' => t('Animals'),
        '#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
      );
    }
    return $view;
  }

  /**
   * Returns edit form.
   *
   * @param array $form
   *   Form array.
   * @param array $form_state
   *   Form state array.
   *
   * @return array
   *   Form array.
   */
  public function getEditForm(array $form, array $form_state) {
    $form = parent::getEditForm($form, $form_state);
    $form['unit'] = array(
      '#type' => 'textfield',
      '#title' => t('Unit'),
      '#default_value' => $this->getUnit(),
      '#maxlength' => 32,
      '#required' => TRUE,
      '#weight' => 1,
    );
    $form['target'] = array(
      '#type' => 'textfield',
      '#title' => t('Target'),
      '#default_value' => $this->getTarget(),
      '#maxlength' => 32,
      '#required' => TRUE,
      '#weight' => 1,
    );
    return $form;
  }

  /**
   * Submits edit form state.
   *
   * @param array $form_state
   *   Form state array.
   */
  public function getEditFormSubmit(array $form_state) {
    parent::getEditFormSubmit($form_state);
    $this->setUnit($form_state['values']['unit']);
    $this->setTarget($form_state['values']['target']);
  }

  /**
   * Sets unit for this object.
   *
   * @param string $unit
   *   Form state array.
   */
  public function setUnit($unit) {
    $this->unit = check_markup($unit, 'filtered_html');
  }

  /**
   * Returns unit for this object.
   *
   * @return array
   *   Array of objects of type CatriaDescribes.
   */
  public function getUnit() {
    if (isset($this->unit)) {
      return check_markup($this->unit, 'filtered_html');
    }
    return FALSE;
  }

  /**
   * Sets target for this object.
   *
   * @param string $target
   *   Target string.
   */
  public function setTarget($target) {
    $this->target = check_plain($target);
  }

  /**
   * Returns target for this object.
   *
   * @return string
   *   Target string.
   */
  public function getTarget() {
    if (isset($this->target)) {
      return check_plain($this->target);
    }
    return FALSE;
  }

  /**
   * Sets measurement for this object.
   *
   * @param CatriaMeasurement $measurement
   *   Object of type CatriaMeasurement.
   */
  public function setMeasurement(CatriaMeasurement $measurement) {
    $this->measurementid = $measurement->getId();
  }

  /**
   * Returns measurement for this object.
   *
   * @return CatriaMeasurement
   *   Object of type CatriaMeasurement.
   */
  public function getMeasurement() {
    if (isset($this->measurementid)) {
      return entity_get_controller('catria_measurement')->loadSingle($this->measurementid);
    }
    return FALSE;
  }

  /**
   * Returns determines for this object.
   *
   * @return CatriaDetermines
   *   Object of type CatriaDetermines.
   */
  public function getDeterminess() {
    if ($this->getId()) {
      return entity_get_controller('catria_determines')->getDeterminessByAttribute($this);
    }
    return FALSE;
  }

  /**
   * Deletes determines for this object.
   */
  public function deleteDeterminess() {
    if ($determiness = $this->getDeterminess()) {
      foreach ($determiness as $determines) {
        $determines->delete();
      }
    }
  }

  /**
   * Deletes this object.
   */
  public function delete() {
    if (isset($this->id)) {
      $this->deleteDeterminess();
      entity_get_controller('catria_attribute')->delete(array($this->id));
    }
  }

}
