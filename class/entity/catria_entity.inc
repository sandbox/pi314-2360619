<?php

/**
 * @file
 * Contains the CatriaAttribute class.
 */

/**
 * Represents Catria entity-objects.
 */
class CatriaEntity extends Entity {

  const OVERVIEW_PAGE = '/';
  const DEFAULT_URI = 'catria/entity/';
  const EDIT_FORM_CALLBACK = 'catria_entity_edit_form';
  public $name;
  public $description;
  public $id;

  /**
   * Returns default label.
   *
   * @return string
   *   A label string.
   */
  public function defaultLabel() {
    return $this->getName();
  }

  /**
   * Returns view array for this object.
   *
   * @param string $view_mode
   *   Parameter for parent function.
   * @param string $langcode
   *   Parameter for parent function.
   * @param string $page
   *   Parameter for parent function.
   *
   * @return array
   *   A view array.
   */
  public function view($view_mode = 'full', $langcode = NULL, $page = NULL) {
    $view = array();
    if (!empty($this->description)) {
      $view['description'] = array('#markup' => t('<b>Description:</b>') . $this->description);
    }
    return $view;
  }

  /**
   * Returns edit form.
   *
   * @param array $form
   *   A form array.
   * @param array $form_state
   *   A form state array.
   *
   * @return array
   *   A form array.
   */
  public function getEditForm(array $form, array $form_state) {
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#default_value' => $this->getName(),
      '#maxlength' => 250,
      '#required' => TRUE,
      '#weight' => 0,
    );
    $form['description'] = array(
      '#type' => 'textarea',
      '#title' => t('Description'),
      '#default_value' => $this->getDescription(),
      '#maxlength' => 3000,
      '#required' => TRUE,
      '#weight' => 1,
    );
    $form['data']['#tree'] = TRUE;
    $form['actions'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('form-actions')),
      '#weight' => 400,
    );
    $submit = array();
    if (!empty($form['#submit'])) {
      $submit += $form['#submit'];
    }
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
      '#submit' => $submit + array('catria_entity_edit_form_submit'),
    );
    return $form;
  }


  /**
   * Submits edit form state.
   *
   * @param array $form_state
   *   A form state array.
   */
  public function getEditFormSubmit(array $form_state) {
    $this->setName($form_state['values']['name']);
    $this->setDescription($form_state['values']['description']);
    if ($this->is_new = isset($this->is_new) ? $this->is_new : 0) {
      $this->created = time();
    }
    $this->changed = time();
    $this->save();
    return TRUE;
  }

  /**
   * Returns delete form.
   *
   * @return array
   *   Form array.
   */
  public function getDeleteForm() {
    $form['#submit'][] = 'catria_entity_delete_form_submit';
    $question = t('Are you sure you want to delete this entity?') . ' ' . $this->name;
    $description = t('This action cannot be undone');
    $form = confirm_form($form, $question, NULL, $description, 'delete', 'cancel', 'confirm');
    return $form;
  }

  /**
   * Returns edit callback function.
   *
   * @return string
   *   A string with edit callback function.
   */
  public function getEditFormCallback() {
    return ($this::EDIT_FORM_CALLBACK);
  }

  /**
   * Returns overview page.
   *
   * @return string
   *   A string with edit callback function.
   */
  public function getOverviewPage() {
    return $this::OVERVIEW_PAGE;
  }

  /**
   * Returns default Uri.
   *
   * @return array
   *   An array with uri.
   */
  public function defaultUri() {
    return array('path' => $this::DEFAULT_URI . $this->getId());
  }

  /**
   * Sets id.
   *
   * @param int $id
   *   An integer with id.
   */
  public function setid($id) {
    $this->id = $id;
    $this->save();
  }

  /**
   * Returns id.
   *
   * @return int|FALSE
   *   An integer with id.
   */
  public function getId() {
    if (isset($this->id)) {
      return $this->id;
    }
    return FALSE;
  }

  /**
   * Sets name.
   *
   * @param string $name
   *   A string with name.
   */
  public function setName($name) {
    $this->name = check_plain($name);
  }

  /**
   * Returns name.
   *
   * @return string|FALSE
   *   A string with name.
   */
  public function getName() {
    if (isset($this->name)) {
      return check_plain($this->name);
    }
    return FALSE;
  }

  /**
   * Sets string description.
   *
   * @param string $description
   *   A string with description.
   */
  public function setDescription($description) {
    $this->description = check_markup($description, 'filtered_html');
  }

  /**
   * Returns description.
   *
   * @return string
   *   A string with description.
   */
  public function getDescription() {
    if (isset($this->description)) {
      return check_markup($this->description, 'filtered_html');
    }
    return FALSE;
  }

  /**
   * Sets feedingtrial-object.
   *
   * @param CatriaFeedingtrial $feedingtrial
   *   An object of type CatriaFeedingtrial.
   */
  public function setFeedingtrial(CatriaFeedingtrial $feedingtrial) {
    $this->feedingtrialid = $feedingtrial->getId();
  }

  /**
   * Returns feedingtrial-object.
   *
   * @return object|FALSE
   *   An object of type CatriaFeedingtrial.
   */
  public function getFeedingtrial() {
    if (isset($this->feedingtrialid)) {
      return entity_get_controller('catria_feedingtrial')->loadSingle($this->feedingtrialid);
    }
    return FALSE;
  }

  /**
   * Sets variable-object.
   *
   * @param CatriaVariable $variable
   *   An object of type CatriaVariable.
   */
  public function setVariable(CatriaVariable $variable) {
    $this->variableid = $variable->getId();
  }

  /**
   * Returns variable-object.
   *
   * @return object|FALSE
   *   An object of type CatriaFeedingtrial.
   */
  public function getVariable() {
    if (isset($this->variableid)) {
      return entity_get_controller('catria_variable')->loadSingle($this->variableid);
    }
    return FALSE;
  }

  /**
   * Sets user-object.
   *
   * @param object $user
   *   An object of type User.
   */
  public function setUser($user) {
    $this->userid = $user->uid;
  }

  /**
   * Returns user-object.
   *
   * @return object|FALSE
   *   An object of type User.
   */
  public function getUser() {
    if (isset($this->userid)) {
      return user_load($this->userid);
    }
    return FALSE;
  }

  /**
   * Sets animal-object.
   *
   * @param CatriaAnimal $animal
   *   An object of type CatriaAnimal.
   */
  public function setAnimal(CatriaAnimal $animal) {
    $this->animalid = $animal->getId();
  }

  /**
   * Returns animal-object.
   *
   * @return object|FALSE
   *   An object of type CatriaAnimal.
   */
  public function getAnimal() {
    if (isset($this->animalid)) {
      return entity_get_controller('catria_animal')->loadSingle($this->animalid);
    }
    return FALSE;
  }

  /**
   * Sets measurement-object.
   *
   * @param CatriaMeasurement $measurement
   *   An object of type CatriaMeasurement.
   */
  public function setMeasurement(CatriaMeasurement $measurement) {
    $this->measurementid = $measurement->getId();
  }

  /**
   * Returns measurement-object.
   *
   * @return object|FALSE
   *   An object of type CatriaMeasurement.
   */
  public function getMeasurement() {
    if (isset($this->measurementid)) {
      return entity_get_controller('catria_measurement')->loadSingle($this->measurementid);
    }
    return FALSE;
  }

  /**
   * Sets attribute-object.
   *
   * @param CatriaAttribute $attribute
   *   An object of type CatriaAttribute.
   */
  public function setAttribute(CatriaAttribute $attribute) {
    $this->attributeid = $attribute->getId();
  }

  /**
   * Returns attribute-object.
   *
   * @return object|FALSE
   *   An object of type CatriaAttribute.
   */
  public function getAttribute() {
    if (isset($this->attributeid)) {
      return entity_get_controller('catria_attribute')->loadSingle($this->attributeid);
    }
    return FALSE;
  }

}
