<?php

/**
 * @file
 * This file contains the CatriaAttribute class.
 */

/**
 * Represents Catria attribute-objects.
 */
class CatriaMeasurement extends CatriaEntity {

  const ADDTITLE = 'Add a new measurement';
  const OVERVIEW_PAGE = 'catria/mymeasurements/';
  const DEFAULT_URI  = 'catria/measurement/';
  public $publications;
  public $userid;
  public $status;
  public $feedingtrialid;
  public $validation;

  /**
   * Returns view array.
   *
   * @param string $view_mode
   *   Parameter for parent function.
   * @param string $langcode
   *   Parameter for parent function.
   * @param string $page
   *   Parameter for parent function.
   *
   * @return array
   *   A view array.
   */
  public function view($view_mode = 'full', $langcode = NULL, $page = NULL) {
    $view = parent::view();
    if ($this->getPublications()) {
      $view['publication'] = array(
        '#markup' => t('<b>Publication: </b>') . $this->getPublications(),
      );
    }
    if ($this->getFeedingtrial()) {
      $view['feedingtrial'] = array(
        '#markup' => t('<b>Feedingtrial: </b>') . l($this->getFeedingtrial()->getName(), 'catria/feedingtrial/' . $this->getFeedingtrial()->getId()),
      );
    }
    if ($this->getStatus() != 'empty') {
      $attributes_header = array(t('Name'), t('Unit'), t('Description'));
      if ($attributes = $this->getAttributes()) {
        foreach ($attributes as $attribute) {
          $attributes_rows[$attribute->getId()] = array(l($attribute->getName(), 'catria/attribute/' . $attribute->getId()),
            $attribute->getUnit(),
            $attribute->getDescription(),
          );
        }
        $view['attributes'] = array(
          '#type' => 'item',
          '#title' => t('Attributes'),
          '#markup' => theme('table', array('header' => $attributes_header, 'rows' => $attributes_rows)),
        );
      }
    }
    elseif ($this->getStatus() == 'empty') {
      if (catria_edit_measurement_access('use catria intern', $this)) {
        $view['files'] = drupal_get_form('catria_measurement_upload_files_form', $this);
      }
    }
    return $view;
  }

  /**
   * Returns edit form.
   *
   * @param array $form
   *   Form array.
   * @param array $form_state
   *   Form state array.
   *
   * @return array
   *   Form array.
   */
  public function getEditForm(array $form, array $form_state) {
    $form = parent::getEditForm($form, $form_state);
    if (!$this->getFeedingtrial() && $feedingtrials = entity_get_controller('catria_feedingtrial')->getEntities()) {
      $dropdown_array = array();
      foreach ($feedingtrials as $feedingtrial) {
        $dropdown_array[$feedingtrial->getId()] = $feedingtrial->getName();
      }
      $form['feedingtrial'] = array(
        '#type' => 'select',
        '#multiple' => FALSE,
        '#options' => $dropdown_array,
        '#title' => t('Feedingtrial'),
        '#required' => TRUE,
      );
    }
    $form['publications'] = array(
      '#type' => 'textarea',
      '#title' => t('Publications'),
      '#default_value' => $this->getPublications(),
      '#maxlength' => 3000,
      '#required' => TRUE,
      '#weight' => 1,
    );
    return $form;
  }

  /**
   * Submits edit form state.
   *
   * @param array $form_state
   *   Form state array.
   */
  public function getEditFormSubmit(array $form_state) {
    parent::getEditFormSubmit($form_state);
    global $user;
    if (!$this->getFeedingtrial()) {
      $feedingtrial = entity_get_controller('catria_feedingtrial')->loadSingle($form_state['values']['feedingtrial']);
      $this->setFeedingtrial($feedingtrial);
    }
    $this->setPublications($form_state['values']['publications']);
    if (!$this->getStatus()) {
      $this->setStatus('empty');
    }
    $this->setHas($user, 'administrator');
    return $form_state;
  }

  /**
   * Returns upload files form.
   *
   * @param array $form
   *   Form array.
   * @param array $form_state
   *   Form state array.
   *
   * @return array
   *   Form array.
   */
  public function getUploadFilesForm(array $form, array $form_state) {
    $form['attributefile'] = array(
      '#type' => 'managed_file',
      '#title' => t('attribute file'),
      '#upload_validators' => array('file_validate_extensions' => array('csv')),
      '#upload_location' => 'private://measurement/' . $this->getName() . '/',
    );
    $form['datafile'] = array(
      '#type' => 'managed_file',
      '#title' => t('data data'),
      '#upload_validators' => array('file_validate_extensions' => array('csv')),
      '#upload_location' => 'private://measurement/' . $this->getName() . '/',
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Integrate files'),
      '#submit' => array('catria_measurement_upload_files_form_submit'),
      '#validate' => array('catria_measurement_upload_files_form_validate'),
    );
    return $form;
  }

  /**
   * Returns request form.
   *
   * @param array $form
   *   Form array.
   * @param array $form_state
   *   Form state array.
   *
   * @return array
   *   Form array.
   */
  public function getRequestForm(array $form, array $form_state) {
    global $user;
    if ($this->getHasAccess($user) == 'administrator') {
      $form['message'] = array('#markup' => t('You do not need to request access. You are the adminitrator of this measurement'));
      return $form;
    }
    else {
      if ($this->getStatus() == 'restricted') {
        if ($this->getHasAccess($user) == 'no access') {
          $form['message'] = array('#markup' => t('The access to the measurement is restricted. You can send a request to the administrators of this measurement below:'));
          $form['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Send request'),
            '#submit' => array('catria_measurement_request_form_submit'),
          );
        }
        if ($this->getHasAccess($user) == 'requested') {
          $form['message'] = array('#markup' => t('You have already requested access to the measurement.'));
        }
        if ($this->getHasAccess($user) == 'allowed') {
          $form['message'] = array('#markup' => t('You have already access to the measurement.'));
        }
      }
      return $form;
    }
  }

  /**
   * Returns manage status form.
   *
   * @param array $form
   *   Form array.
   * @param array $form_state
   *   Form state array.
   *
   * @return array
   *   Form array.
   */
  public function getManageStatusForm(array $form, array $form_state) {
    if ($hass = $this->getHass()) {
      $header = array('User', 'Access', 'allow', 'refuse');
      $rows = array();
      foreach ($hass as $has) {
        $allow = array();
        $refuse = array();
        if ($has->getAccess() == 'requested') {
          $refuse = l(t('refuse'), 'catria/measurement/' . $this->getId() . '/status/refuse/' . $has->userid);
          $allow = l(t('allow'), 'catria/measurement/' . $this->getId() . '/status/allow/' . $has->userid);
        }
        if ($has->getAccess() == 'allowed') {
          $refuse = l(t('refuse'), 'catria/measurement/' . $this->getId() . '/status/refuse/' . $has->userid);
        }
        $rows[] = array(l($has->userid, 'user/' . $has->userid),
          $has->getAccess(),
          $allow,
          $refuse,
        );
      }
      $form['users'] = array(
        '#type' => 'item',
        '#title' => t('Users'),
        '#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
      );
    }
    $dropdown_array = array(
      'closed' => 'closed',
      'restricted' => 'restricted',
      'intern' => 'intern',
      'open' => 'open',
    );
    $form['status'] = array(
      '#type' => 'select',
      '#multiple' => FALSE,
      '#default_value' => $this->getStatus(),
      '#options' => $dropdown_array,
      '#title' => t('Status') . ': ' . $this->getStatus(),
      '#required' => TRUE,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('set new status'),
      '#submit' => array('catria_measurement_manage_status_form_submit'),
    );
    return $form;
  }

  /**
   * Validates attributefile and datafile.
   *
   * @param object $attributefile
   *   File object.
   * @param object $datafile
   *   File object.
   *
   * @return array
   *   An array with validation results.
   */
  public function validateFiles($attributefile, $datafile) {
    $valid = TRUE;
    $errors = array();
    $attribute_fp = fopen($attributefile->uri, 'r');
    $attribute_colnames = fgetcsv($attribute_fp, 0, ',');
    if (count($attribute_colnames) != 4) {
      $errors[] = t('attribute file: wrong number of column names');
      $valid = FALSE;
    }
    else {
      if (!($attribute_colnames[0] == 'name' && $attribute_colnames[1] == 'unit' && $attribute_colnames[2] == 'target' && $attribute_colnames[3] == 'description')) {
        $errors[] = t('attribute file: column names or order not valid');
        $valid = FALSE;
      }
      else {
        $attribute_names = array();
        $templine = 1;
        while (($attribute_line = fgetcsv($attribute_fp, 0, ',')) !== FALSE) {
          if (count($attribute_line) != 4) {
            $errors[] = t('attribute file: line is not valid') . ' ' . $templine;
            $valid = FALSE;
          }
          $attribute_names[$attribute_line[0]] = $attribute_line[0];
          $templine++;
        }
        $data_fp = fopen($datafile->uri, 'r');
        $data_colnames = fgetcsv($data_fp, 0, ',');
        $data_colnum = count($data_colnames);
        if ($data_colnum != count($attribute_names) + 2) {
          $errors[] = t('data file: wrong number of columns');
          $errors[] = $data_colnames;
          $errors[] = $attribute_names;
          $valid = FALSE;
        }
        else {
          for ($i = 2; $i < $data_colnum; $i++) {
            if (!isset($attribute_names[$data_colnames[$i]])) {
              $errors[] = t('data file: attribute is not in attribute file') . ' ' . $data_colnames[$i];
              $valid = FALSE;
            }
          }
          $animal_names = array();
          while (($temp_line = fgetcsv($data_fp, 0, ',')) !== FALSE) {
            if ($data_colnum != count($temp_line)) {
              $valid = FALSE;
              $errors[] = t('table is incomplete');
            }
            else {
              $animal_names[] = $temp_line[0];
            }
          }
          $animal_controller = new CatriaAnimalController('catria_animal');
          for ($i = 0; $i < count($animal_names); $i++) {
            $animal = $animal_controller->loadAnimalByName($animal_names[$i], $this->getFeedingtrial());
            if (!$animal) {
              $valid = FALSE;
              $errors[] = t('data file: animal does not exist in the feedingtrial') . ' ' . $animal_names[$i];
            }
          }
        }
      }
    }
    return array('valid' => $valid, 'errors' => $errors);
  }

  /**
   * Integrates attributefile and datafile.
   *
   * @param object $attributefile
   *   File object.
   * @param object $datafile
   *   File object.
   */
  public function integrateFiles($attributefile, $datafile) {
    $attribute_fp = fopen($attributefile->uri, 'r');
    $temp_line = fgetcsv($attribute_fp, 0, ',');
    $attributes = array();
    $attribute_controller = new CatriaAttributeController('catria_attribute');
    while (($temp_line = fgetcsv($attribute_fp, 0, ',')) !== FALSE) {
      $attribute = $attribute_controller->create();
      $attribute->setName($temp_line[0]);
      $attribute->setUnit($temp_line[1]);
      $attribute->setTarget($temp_line[2]);
      $attribute->setDescription($temp_line[3]);
      $attribute->setMeasurement($this);
      $attribute->save();
      $attributes[$temp_line[0]] = $attribute;
    }
    $animals_timepoints_attributes_values = array();
    $animal_names = array();
    $data_fp = fopen($datafile->uri, 'r');
    $data_colnames = fgetcsv($data_fp, 0, ',');
    $animal_controller = new CatriaAnimalController('catria_animal');
    $feedingtrial = $this->getFeedingtrial();
    while (($temp_line = fgetcsv($data_fp, 0, ',')) !== FALSE) {
      $animal_name = $temp_line[0];
      $animal_names[$animal_name] = $animal_name;
      $timepoint = $temp_line[1];
      for ($i = 2; $i < count($temp_line); $i++) {
        $tmp_value = $temp_line[$i];
        if (!empty($tmp_value)) {
          $animals_timepoints_attributes_values[$animal_name][$timepoint][$data_colnames[$i]] = $tmp_value;
        }
        elseif (is_numeric($tmp_value)) {
          $animals_timepoints_attributes_values[$animal_name][$timepoint][$data_colnames[$i]] = $tmp_value;
        }
      }
    }
    $animals = $animal_controller->loadAnimalByNames($animal_names, $feedingtrial);
    $this->setDeterminess($animals, $attributes, $animals_timepoints_attributes_values);
    $this->setStatus('closed');
    $this->save();
  }

  /**
   * Sets publications.
   *
   * @param string $publications
   *   A string with publications.
   */
  public function setPublications($publications) {
    $this->publications = check_markup($publications, 'filtered_html');
  }

  /**
   * Returns describes-objects.
   *
   * @return array|FALSE
   *   An array with objects of type CatriaDescribes
   */
  public function getPublications() {
    if (isset($this->publications)) {
      return check_markup($this->publications, 'filtered_html');
    }
    return FALSE;
  }

  /**
   * Sets status.
   *
   * @param string $status
   *   A string with status.
   */
  public function setStatus($status) {
    $this->status = $status;
  }

  /**
   * Returns status.
   *
   * @return string|FALSE
   *   A string with status.
   */
  public function getStatus() {
    if (isset($this->status)) {
      return $this->status;
    }
    return FALSE;
  }

  /**
   * Sets has-object.
   *
   * @param object $user
   *   An object of type User.
   * @param string $access
   *   A string with access.
   */
  public function setHas($user, $access) {
    if ($this->getId()) {
      entity_get_controller('catria_has')->setHasByMeasurement($this, $user, $access)->save();
    }
  }

  /**
   * Deletes has-object.
   *
   * @param object $user
   *   An object of type User.
   */
  public function removeHas($user) {
    if ($this->getId()) {
      return entity_get_controller('catria_has')->deleteHassByMeasurementUser($this, $user);
    }
  }

  /**
   * Returns has-object.
   *
   * @return array|FALSE
   *   An array with objects of type CatriaHas
   */
  public function getHass() {
    if ($this->getId()) {
      return entity_get_controller('catria_has')->getHassByMeasurement($this);
    }
    return FALSE;
  }

  /**
   * Returns access.
   *
   * @param object $user
   *   An object of type User.
   *
   * @return string|FALSE
   *   A string with access.
   */
  public function getHasAccess($user) {
    if ($this->getId()) {
      if ($has = entity_get_controller('catria_has')->getHassByMeasurementUser($this, $user)) {
        return $has->getAccess();
      }
      else {
        return 'no access';
      }
    }
    else {
      return FALSE;
    }
  }

  /**
   * Returns number of requests.
   *
   * @return int|FALSE
   *   A number of requests.
   */
  public function getRequestsNum() {
    if ($this->getId()) {
      $hass = entity_get_controller('catria_has')->getHassByMeasurement($this, 'requested');
      return count($hass);
    }
    return FALSE;
  }

  /**
   * Returns animal-objects.
   *
   * @return array
   *   An array with objects of type CatriaAnimal
   */
  public function getAnimals() {
    if ($this->getId()) {
      if ($determiness = entity_get_controller('catria_determines')->getDeterminessByMeasurement($this)) {
        $animals = array();
        foreach ($determiness as $determines) {
          if (!isset($animals[$determines->animalid])) {
            $animals[$determines->animalid] = $determines->getAnimal();
          }
        }
        return $animals;
      }
    }
    else {
      return FALSE;
    }
  }

  /**
   * Returns describess associated .
   *
   * @return array
   *   Array with objects of type CatriaDescribes
   */
  public function getAttributes() {
    if ($this->getId()) {
      return entity_get_controller('catria_attribute')->loadAttributesByMeasurement($this);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Sets determines.
   *
   * @param array $animals
   *   An array with objects of type CatriaAnimals.
   * @param array $attributes
   *   An array with objects of type CatriaAttributes.
   * @param array $animals_timepoints_attributes_values
   *   An array with values.
   */
  public function setDeterminess(array $animals, array $attributes, array $animals_timepoints_attributes_values) {
    if ($this->getId()) {
      entity_get_controller('catria_determines')->setDeterminess($animals, $attributes, $animals_timepoints_attributes_values);
    }
  }

  /**
   * Returns determines.
   *
   * @return array|FALSE
   *   An array with objects of type CatriaDetermines
   */
  public function getDeterminess() {
    if ($this->getId()) {
      return entity_get_controller('catria_determines')->getDeterminessByMeasurement($this);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Deletes attributes.
   */
  public function deleteAttributes() {
    if ($attributes = $this->getAttributes()) {
      foreach ($attributes as $attribute) {
        $attribute->delete();
      }
    }
  }

  /**
   * Deletes has.
   */
  public function deleteHass() {
    if ($hass = $this->getHass()) {
      foreach ($hass as $has) {
        $has->delete();
      }
    }
  }

  /**
   * Deletes object.
   */
  public function delete() {
    if (isset($this->id)) {
      $this->deleteAttributes();
      $this->deleteHass();
      entity_get_controller('catria_measurement')->delete(array($this->id));
    }
  }

  /**
   * Returns default Uri.
   *
   * @return array
   *   An array with uri.
   */
  public function defaultUri() {
    return array('path' => 'catria/feedingtrial/' . $this->feedingtrialid . '/measurement/' . $this->getId());
  }

}
