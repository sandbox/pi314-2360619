<?php

/**
 * @file
 * This file contains the CatriaDescribes class.
 */

/**
 * Represents Catria describes-objects.
 */
class CatriaDescribes extends CatriaEntity {

  public $id;
  public $animalid;
  public $variableid;
  public $value;

  /**
   * Sets value.
   *
   * @param int $value
   *   A value string.
   */
  public function setValue($value) {
    $this->value = $value;
  }

  /**
   * Returns value.
   *
   * @return string
   *   A value string.
   */
  public function getValue() {
    if (isset($this->value)) {
      return $this->value;
    }
    return FALSE;
  }

}
