<?php

/**
 * @file
 * This file contains the CatriaDownload class.
 */

/**
 * Class for Catria download-objects.
 */
class CatriaDownload {

  /**
   * Returns download form.
   *
   * @param array $form
   *   Form array.
   * @param array $form_state
   *   Form state array.
   *
   * @return array
   *   Form array.
   */
  public function getDownloadForm(array $form, array $form_state) {
    $feedingtrial = $form_state['catria_feedingtrial'];
    if ($variables = $feedingtrial->getVariables()) {
      $variable_array = array();
      foreach ($variables as $variable) {
        $variable_array[$variable->getId()] = $variable->getName();
      }
    }
    if ($measurements = $feedingtrial->getMeasurements()) {
      $measurement_array = array();
      foreach ($measurements as $measurement) {
        $measurement_array[$measurement->getId()] = $measurement->getName();
      }
      $form['catria_variables'] = array(
        '#type' => 'checkboxes',
        '#options' => $variable_array,
        '#title' => t('Select variables'),
      );
      $form['catria_measurements'] = array(
        '#type' => 'checkboxes',
        '#options' => $measurement_array,
        '#title' => t('Select mesaurements'),
      );
      $form['actions']['download'] = array(
        '#type' => 'submit',
        '#value' => t('Download data'),
        '#submit' => array('catria_download_form_submit'),
      );
    }
    else {
      $form['message'] = array('#markup' => t('No measurements integrated yet'));;
    }
    return $form;
  }

  /**
   * Submits download form state.
   *
   * @param array $form_state
   *   Form state array.
   */
  public function getDownloadFormSubmit(array $form_state) {
    $feedingtrial = $form_state['catria_feedingtrial'];
    $measurements = entity_get_controller('catria_measurement')->load(array_filter($form_state['values']['catria_measurements']));
    $variables = entity_get_controller('catria_variable')->load(array_filter($form_state['values']['catria_variables']));
    $animals = $feedingtrial->getAnimals();
    $measurements_data = array();
    foreach ($measurements as $measurement) {
      $tmp_attributes = $measurement->getAttributes();
      foreach ($tmp_attributes as $tmp_attribute) {
        $attribute_names[] = $tmp_attribute->getName();
        $tmp_determiness = $tmp_attribute->getDeterminess();
        foreach ($tmp_determiness as $tmp_determines) {
          $measurements_data[$tmp_determines->getAnimal()->getName()][$tmp_determines->getTimepoint()][$tmp_attribute->getName()] = $tmp_determines->getValue();
        }
      }
    }
    $animal_data = array();
    $variable_names = array();
    $describes_controller = entity_get_controller('catria_describes');
    foreach ($variables as $variable) {
      foreach ($animals as $animal) {
        $animal_data[$animal->getName()][$variable->getName()] = $describes_controller->getDescribesByAnimalVariable($animal, $variable)->getValue();
      }
      $variable_names[] = $variable->getName();
    }
    $animal_names = array_keys($measurements_data);
    drupal_add_http_header('Content-Type', 'text/csv');
    drupal_add_http_header('Content-Disposition', 'attachment;filename=csvfile.csv');
    $fp = fopen('php://output', 'w');
    fputcsv($fp, array_merge(array('Animal', 'Timepoint'), $variable_names, $attribute_names));
    foreach ($animal_names as $animal_name) {
      $timepoints = array_keys($measurements_data[$animal_name]);
      foreach ($timepoints as $timepoint) {
        $tmp_line = array();
        $tmp_line[] = $animal_name;
        $tmp_line[] = $timepoint;
        foreach ($variable_names as $variable_name) {
          $tmp_line[] = $animal_data[$animal_name][$variable_name];
        }
        foreach ($attribute_names as $attribute_name) {
          if (isset($measurements_data[$animal_name][$timepoint][$attribute_name])) {
            $tmp_line[] = $measurements_data[$animal_name][$timepoint][$attribute_name];
          }
          else {
            $tmp_line[] = 'NA';
          }
        }
        fputcsv($fp, $tmp_line, ',');
      }
    }
    fclose($fp);
    drupal_exit();
  }

}
