<?php

/**
 * @file
 * This file contains the CatriaVariable class.
 */

/**
 * Class for Catria variable-objects.
 */
class CatriaVariable extends CatriaEntity {

  const DEFAULT_URI  = 'catria/variable/';
  public $feedingtrialid;

  /**
   * Returns a view array for a single animal.
   *
   * @param string $view_mode
   *   Parameter for parent function.
   * @param string $langcode
   *   Parameter for parent function.
   * @param string $page
   *   Parameter for parent function.
   *
   * @return array
   *   View array.
   */
  public function view($view_mode = 'full', $langcode = NULL, $page = NULL) {
    $view = parent::view();
    $view['feedingtrial'] = array(
      '#markup' => t('<b>Feedingtrial: </b>') . l($this->getFeedingtrial()->getName(), 'catria/feedingtrial/' . $this->getFeedingtrial()->getId()),
    );
    return $view;
  }

}
