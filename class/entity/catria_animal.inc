<?php

/**
 * @file
 * Contains the CatriaAnimal class.
 */

/**
 * Represents Catria animal-objects.
 */
class CatriaAnimal extends CatriaEntity {

  public $feedingtrialid;

  /**
   * Returns view array.
   *
   * @param string $view_mode
   *   Parameter for parent function.
   * @param string $langcode
   *   Parameter for parent function.
   * @param string $page
   *   Parameter for parent function.
   *
   * @return array
   *   A view array.
   */
  public function view($view_mode = 'full', $langcode = NULL, $page = NULL) {
    $view = parent::view();
    $view['feedingtrial'] = array(
      '#markup' => t('<b>Feedingtrial</b>:') . l($this->getFeedingtrial()->getName(), 'catria/feedingtrial/' . $this->getFeedingtrial()->getId()),
    );
    if ($describess = $this->getDescribess()) {
      $header = array('Variable', 'value');
      $rows = array();
      foreach ($describess as $describes) {
        $variable = $describes->getVariable();
        $rows[] = array(
          l($variable->getName(), 'catria/variable/' . $variable->getId()),
          $describes->getValue(),
        );
      }
      $view['variables'] = array(
        '#type' => 'item',
        '#title' => t('Experimental variables'),
        '#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
      );
    }
    if ($determiness = $this->getDeterminess()) {
      $measurements = array();
      $header = array(t('Attribute'), t('timepoint'), t('value'));
      $rows = array();
      foreach ($determiness as $determines) {
        $attribute = $determines->getAttribute();
        $measurement = $attribute->getMeasurement();
        $rows[$measurement->getId()][] = array(
          l($attribute->getName(), 'catria/attribute/' . $attribute->getId()),
          $determines->getTimepoint(),
          $determines->getValue(),
        );
        $measurements[$measurement->getId()] = $measurement;
      }
      foreach ($measurements as $measurement) {
        $view['measurement_' . $measurement->getName()] = array(
          '#type' => 'item',
          '#title' => t('Measurement') . ': ' . l($measurement->getName(), 'catria/measurement/' . $measurement->getId()),
          '#markup' => theme('table', array('header' => $header, 'rows' => $rows[$measurement->getId()])),
        );
      }
    }
    return $view;
  }

  /**
   * Returns describes.
   *
   * @return array|FALSE
   *   An array with objects of type CatriaDescribes.
   */
  public function getDescribess() {
    if ($this->getId()) {
      return entity_get_controller('catria_describes')->getDescribessByAnimal($this);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Returns determines.
   *
   * @return array|FALSE
   *   An array with objects of type CatriaDetermines.
   */
  public function getDeterminess() {
    if ($this->getId()) {
      return entity_get_controller('catria_determines')->getDeterminessByAnimal($this);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Returns default Uri.
   *
   * @return array
   *   An array with uri.
   */
  public function defaultUri() {
    return array('path' => 'catria/animal/' . $this->getId());
  }

}
