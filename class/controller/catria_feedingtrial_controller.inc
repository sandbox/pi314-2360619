<?php

/**
 * @file
 * File contains the CatriaFeedingtrialController class.
 */

/**
 * A controller class for Catria feedingtrials.
 */
class CatriaFeedingtrialController extends CatriaEntityController {

  /**
   * Gives an overview of all feedingtrials.
   *
   * @return array
   *   View object.
   */
  public function overview() {
    $header = array(t('Name'), t('Description'), '');
    $view = array();
    $rows = array();
    if ($feedingtrials = $this->getEntities()) {
      foreach ($feedingtrials as $feedingtrial) {
        $rows[] = array(
          l($feedingtrial->getName(), 'catria/feedingtrial/' . $feedingtrial->getId()),
          $feedingtrial->getDescription(),
          l(t('download'), 'catria/feedingtrial/' . $feedingtrial->getId() . '/download/'),
        );
      }
      $view[] = array(
        '#type' => 'item',
        '#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
      );
    }
    return $view;
  }

  /**
   * Gives an overview of all feedingtrials which belong to the user.
   *
   * @return array
   *   View object.
   */
  public function overviewUser() {
    global $user;
    $header = array(t('Name'), t('Description'));
    $view = array();
    $rows = array();
    if ($feedingtrials = $this->loadFeedingtrialsByUser($user)) {
      foreach ($feedingtrials as $feedingtrial) {
        $rows[] = array(
          l($feedingtrial->getName(), 'catria/feedingtrial/' . $feedingtrial->getId()),
          $feedingtrial->getDescription(),
        );
      }
      $view[] = array(
        '#type' => 'item',
        '#title' => t('Feedingtrials'),
        '#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
      );
    }
    return $view;
  }

  /**
   * Loads all feedingtrials which belong to the user.
   *
   * @param object $user
   *   An object of type CatriaFeedingtrial.
   *
   * @return array
   *   An array of objects of type CatriaFeedingtrial.
   */
  public function loadFeedingtrialsByUser($user) {
    $query = new EntityFieldQuery();
    $query
      ->entityCondition('entity_type', 'catria_feedingtrial')
      ->propertyCondition('userid', $user->uid);
    $result = $query->execute();
    if (count($result) > 0) {
      return $this->load(array_keys($result['catria_feedingtrial']));
    }
    return FALSE;
  }

}
