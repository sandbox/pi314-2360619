<?php

/**
 * @file
 * File contains the CatriaDescribesController class.
 */

/**
 * A controller class for Catria describes.
 */
class CatriaDescribesController extends CatriaEntityController {

  /**
   * Creates describes by an animal and a set of variables.
   *
   * @param CatriaAnimal $animal
   *   An object of type CatriaAnimal.
   * @param CatriaVariable $variables
   *   An array of object of type CatriaVariables.
   *
   * @return bool
   *   Boolean value
   */
  public function setDescribessByAnimal(CatriaAnimal $animal, CatriaVariable $variables) {
    $this->deleteDescribessByAnimal($animal);
    foreach ($variables as $variable) {
      $describes = $this->create(array());
      $describes->setAnimal($animal);
      $describes->setVariable($variable);
      $describes->setValue(NULL);
      $describes->save();
    }
    return TRUE;
  }

  /**
   * Loads describes by a specific animal.
   *
   * @param CatriaAnimal $animal
   *   An object of type CatriaAnimal.
   *
   * @return array
   *   An array of objects of type CatriaDescribes.
   */
  public function getDescribessByAnimal(CatriaAnimal $animal) {
    $query = new EntityFieldQuery();
    $query
      ->entityCondition('entity_type', 'catria_describes')
      ->propertyCondition('animalid', $animal->getId());
    $result = $query->execute();
    if (count($result) > 0) {
      $describess = $this->load(array_keys($result['catria_describes']));
      if ($describess) {
        return $describess;
      }
    }
    return FALSE;
  }

  /**
   * Deletes describes by a specific animal.
   *
   * @param CatriaAnimal $animal
   *   An object of type CatriaAnimal.
   *
   * @return bool
   *   Boolean value.
   */
  public function deleteDescribessByAnimal(CatriaAnimal $animal) {
    $query = new EntityFieldQuery();
    $query
      ->entityCondition('entity_type', 'catria_describes')
      ->propertyCondition('animalid', $animal->getId());
    $result = $query->execute();
    if (count($result) > 0) {
      $describess = $this->load(array_keys($result['catria_describes']));
      if ($describess) {
        foreach ($describess as $describes) {
          $describes->delete();
        }
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Creates describes for a animal and variable and sets value.
   *
   * @param CatriaAnimal $animal
   *   An object of type CatriaAnimal.
   * @param CatriaVariable $variable
   *   An object of type CatriaVariable.
   * @param int $value
   *   A value.
   *
   * @return CatriaDescribes
   *   Object of type CatriaDescribes.
   */
  public function setDescribesValue(CatriaAnimal $animal, CatriaVariable $variable, $value) {
    $describes = $this->create(array());
    $describes->animalid = $animal->id;
    $describes->variableid = $variable->id;
    $describes->value = $value;
    return $describes;
  }

  /**
   * Loads describes for a specific animal and a specific variable.
   *
   * @param CatriaAnimal $animal
   *   An object of type CatriaAnimal.
   * @param CatriaVariable $variable
   *   An object of type CatriaVariable.
   *
   * @return array
   *   Object of type CatriaDescribes.
   */
  public function getDescribesByAnimalVariable(CatriaAnimal $animal, CatriaVariable $variable) {
    $query = new EntityFieldQuery();
    $query
      ->entityCondition('entity_type', 'catria_describes')
      ->propertyCondition('animalid', $animal->getId())
      ->propertyCondition('variableid', $variable->getId());
    $result = $query->execute();
    if (count($result) > 0) {
      $describesid = array_keys($result['catria_describes'])[0];
      return $this->loadSingle($describesid);
    }
    return FALSE;
  }

}
