<?php

/**
 * @file
 * File contains the CatriaDeterminesController class.
 */

/**
 * A controller class for Catria determines.
 */
class CatriaDeterminesController extends CatriaEntityController {

  /**
   * Sets determines by attributes, animals and values.
   *
   * @param array $animals
   *   An array with objects of type CatriaAnimal.
   * @param array $attributes
   *   An array with objects of type CatriaAttributes.
   * @param array $animals_timepoints_attributes_values
   *   An array with values by animals, attributes and timepoints.
   *
   * @return bool
   *   A boolean value.
   */
  public function setDeterminess(array $animals, array $attributes, array $animals_timepoints_attributes_values) {
    foreach ($animals as $animal) {
      $animal_name = $animal->name;
      if (isset($animals_timepoints_attributes_values[$animal_name])) {
        $timepoints = array_keys($animals_timepoints_attributes_values[$animal_name]);
        foreach ($timepoints as $timepoint) {
          $attribute_names = array_keys($animals_timepoints_attributes_values[$animal_name][$timepoint]);
          foreach ($attribute_names as $attribute_name) {
            $attribute = $attributes[$attribute_name];
            $determines = $this->create(array());
            $determines->animalid = $animal->id;
            $determines->attributeid = $attribute->id;
            $determines->timepoint = $timepoint;
            $determines->value = $animals_timepoints_attributes_values[$animal_name][$timepoint][$attribute_name];
            $determines->save();
          }
        }
      }
    }
    return TRUE;
  }

  /**
   * Returns determines-objects which belong to an animal-object.
   *
   * @param CatriaAnimal $animal
   *   An object of type CatriaAnimal.
   *
   * @return array|FALSE
   *   An array with objects of type CatriaDetermines.
   */
  public function getDeterminessByAnimal(CatriaAnimal $animal) {
    $query = new EntityFieldQuery();
    $query
      ->entityCondition('entity_type', 'catria_determines')
      ->propertyCondition('animalid', $animal->getId());
    $result = $query->execute();
    if (isset($result['catria_determines'])) {
      return $this->load(array_keys($result['catria_determines']));
    }
    return FALSE;
  }

  /**
   * Returns determines-objects which belong to an attribute-object.
   *
   * @param CatriaAttribute $attribute
   *   An object of type CatriaAttribute.
   *
   * @return array|FALSE
   *   An array with objects of type CatriaDetermines.
   */
  public function getDeterminessByAttribute(CatriaAttribute $attribute) {
    $query = new EntityFieldQuery();
    $query
      ->entityCondition('entity_type', 'catria_determines')
      ->propertyCondition('attributeid', $attribute->getId());
    $result = $query->execute();
    if (isset($result['catria_determines'])) {
      return $this->load(array_keys($result['catria_determines']));
    }
    return FALSE;
  }

}
