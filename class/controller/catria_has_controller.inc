<?php

/**
 * @file
 * File contains the CatriaHasController class.
 */

/**
 * Controller class for CatriaHas objects.
 */
class CatriaHasController extends CatriaEntityController {

  /**
   * Creates has by measurement, user and access.
   *
   * @param CatriaMeasurement $measurement
   *   An of type CatriaMeasurement.
   * @param object $user
   *   An object of type User.
   * @param string $access
   *   A string with access role.
   *
   * @return object
   *   An object of type CatriaHas.
   */
  public function setHasByMeasurement(CatriaMeasurement $measurement, $user, $access) {
    $this->deleteHassByMeasurementUser($measurement, $user);
    $has = $this->create(array());
    $has->setMeasurement($measurement);
    $has->setUser($user);
    $has->setAccess($access);
    return $has;
  }

  /**
   * Deletes all has by measurement and user.
   *
   * @param CatriaMeasurement $measurement
   *   An of type CatriaMeasurement.
   * @param object $user
   *   An object of type User.
   */
  public function deleteHassByMeasurementUser(CatriaMeasurement $measurement, $user) {
    $query = new EntityFieldQuery();
    $query
      ->entityCondition('entity_type', 'catria_has')
      ->propertyCondition('measurementid', $measurement->getId())
      ->propertyCondition('userid', $user->uid);
    $result = $query->execute();
    if (count($result) > 0) {
      $hass = $this->load(array_keys($result['catria_has']));
      if ($hass) {
        foreach ($hass as $has) {
          $has->delete();
        }
      }
    }
  }

  /**
   * Returns all has by measurement and access.
   *
   * @param CatriaMeasurement $measurement
   *   An of type CatriaMeasurement.
   * @param string $access
   *   A string with access role.
   *
   * @return array|FALSE
   *   An array with objects of type CatriaHas.
   */
  public function getHassByMeasurementAccess(CatriaMeasurement $measurement, $access) {
    $query = new EntityFieldQuery();
    $query
      ->entityCondition('entity_type', 'catria_has')
      ->propertyCondition('measurementid', $measurement->getId())
      ->propertyCondition('access', $access);
    $result = $query->execute();
    if (count($result) > 0) {
      return $this->load(array_keys($result['catria_has']));
    }
    return FALSE;
  }

  /**
   * Loads all has by measurement.
   *
   * @param CatriaMeasurement $measurement
   *   An of type CatriaMeasurement.
   *
   * @return array
   *   An array with objects of type CatriaHas.
   */
  public function getHassByMeasurement(CatriaMeasurement $measurement) {
    $query = new EntityFieldQuery();
    $query
      ->entityCondition('entity_type', 'catria_has')
      ->propertyCondition('measurementid', $measurement->getId());
    $result = $query->execute();
    if (count($result) > 0) {
      return $this->load(array_keys($result['catria_has']));
    }
    return FALSE;
  }

  /**
   * Loads single has by measurement and user.
   *
   * @param CatriaMeasurement $measurement
   *   An of type CatriaMeasurement.
   * @param object $user
   *   An object of type User.
   *
   * @return array
   *   An array with objects of type CatriaHas.
   */
  public function getHassByMeasurementUser(CatriaMeasurement $measurement, $user) {
    $query = new EntityFieldQuery();
    $query
      ->entityCondition('entity_type', 'catria_has')
      ->propertyCondition('measurementid', $measurement->getId())
      ->propertyCondition('userid', $user->uid);
    $result = $query->execute();
    if (count($result) > 0) {
      $hasid = array_keys($result['catria_has'])[0];
      return $this->loadSingle($hasid);
    }
    return FALSE;
  }

}
