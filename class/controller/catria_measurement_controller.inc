<?php

/**
 * @file
 * File contains the CatriaMeasurementController class.
 */

/**
 * A controller class for Catria measurements.
 */
class CatriaMeasurementController extends CatriaEntityController {

  /**
   * Gives an overview of all measurements for specific feedingtrial.
   *
   * @param CatriaFeedingtrial $feedingtrial
   *   An object of type CatriaFeedingtrial.
   *
   * @return array
   *   View object.
   */
  public function overviewFeedingtrial(CatriaFeedingtrial $feedingtrial) {
    $view = array();
    if ($measurements = $this->loadMeasurementsByFeedingtrial($feedingtrial)) {
      $header = array(t('Name'), t('Description'), t('Status'));
      $rows = array();
      foreach ($measurements as $measurement) {
        $rows[$feedingtrial->getId()][] = array(
          l($measurement->getName(), 'catria/measurement/' . $measurement->getId()),
          $measurement->getDescription(), $measurement->getStatus(),
        );
      }
      $view['overview'] = array(
        '#type' => 'item',
        '#title' => t('Measurements'),
        '#markup' => theme('table', array('header' => $header, 'rows' => $rows[$feedingtrial->getId()])),
      );
    }
    return $view;
  }

  /**
   * Gives an overview of all measurements which belong to the user.
   *
   * @return array
   *   View object.
   */
  public function overviewUser() {
    global $user;
    $view = array();
    if ($feedingtrials = entity_get_controller('catria_feedingtrial')->getEntities()) {
      $rows = array();
      $header_admin = array(
        t('Name'),
        t('Feedingtrial'),
        t('Description'),
        t('Status'),
        t('Requests'),
      );
      foreach ($feedingtrials as $feedingtrial) {
        if ($measurements = $this->loadMeasurementsByFeedingtrial($feedingtrial)) {
          foreach ($measurements as $measurement) {
            if ($measurement->getHasAccess($user) == 'administrator') {
              $rows['administrator'][] = array(
                l($measurement->getName(), 'catria/measurement/' . $measurement->getId()),
                l($feedingtrial->getName(), 'catria/feedingtrial/' . $feedingtrial->getId()),
                $measurement->getDescription(), $measurement->getStatus(),
                $measurement->getRequestsNum(),
              );
            }
          }
        }
      }
      if (isset($rows['administrator'])) {
        $view['Administrator'] = array(
          '#type' => 'item',
          '#title' => t('Measurements'),
          '#markup' => theme('table', array('header' => $header_admin, 'rows' => $rows['administrator'])),
        );
      }
    }
    return $view;
  }

  /**
   * Loads all measurements which belong to a specific feedingtrial.
   *
   * @param CatriaFeedingtrial $feedingtrial
   *   An object of type CatriaFeedingtrial.
   *
   * @return array
   *   An array of objects of type CatriaMeasurement.
   */
  public function loadMeasurementsByFeedingtrial(CatriaFeedingtrial $feedingtrial) {
    $query = new EntityFieldQuery();
    $query
      ->entityCondition('entity_type', 'catria_measurement')
      ->propertyCondition('feedingtrialid', $feedingtrial->getId());
    $result = $query->execute();
    if (count($result) > 0) {
      return $this->load(array_keys($result['catria_measurement']));
    }
    return FALSE;
  }

}
