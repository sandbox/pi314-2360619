<?php

/**
 * @file
 * File contains the CatriaAttributeController class.
 */

/**
 * A controller class for Catria attributes.
 */
class CatriaAttributeController extends CatriaEntityController {

  /**
   * Loads all attributes which belong to a specific measurement.
   *
   * @param CatriaMeasurement $measurement
   *   An object of type CatriaMeasurement.
   *
   * @return array
   *   An array of objects of type CatriaAttribute.
   */
  public function loadAttributesByMeasurement(CatriaMeasurement $measurement) {
    $query = new EntityFieldQuery();
    $query
      ->entityCondition('entity_type', 'catria_attribute')
      ->propertyCondition('measurementid', $measurement->getId());
    $result = $query->execute();
    if (count($result) > 0) {
      return $this->load(array_keys($result['catria_attribute']));
    }
    return FALSE;
  }

}
