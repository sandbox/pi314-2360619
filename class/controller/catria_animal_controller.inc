<?php

/**
 * @file
 * Contains the CatriaAnimalController class.
 */

/**
 * Controller for CatriaAnimal objects.
 */
class CatriaAnimalController extends CatriaEntityController {

  /**
   * Gives an overview of all animals for a specific feedingtrial.
   *
   * @param CatriaFeedingtrial $feedingtrial
   *   An object of type CatriaFeedingtrial.
   *
   * @return array
   *   View array.
   */
  public function overviewFeedingtrial(CatriaFeedingtrial $feedingtrial) {
    $header = array();
    $header[] = t('Name');
    if ($variables = entity_get_controller('catria_variable')->loadVariablesByFeedingtrial($feedingtrial)) {
      foreach ($variables as $variable) {
        $header[] = $variable->getName();
      }
    }
    $view = array();
    $rows = array();
    if ($animals = $this->loadAnimalsByFeedingtrial($feedingtrial)) {
      $describes_controller = entity_get_controller('catria_describes');
      foreach ($animals as $animal) {
        $tmp_row = array();
        $tmp_row[] = l($animal->name, 'catria/animal/' . $animal->id);
        foreach ($variables as $variable) {
          $tmp_row[] = $describes_controller->getDescribesByAnimalVariable($animal, $variable)->getValue();
        }
        $rows[] = $tmp_row;
      }
      $view[] = array(
        '#type' => 'item',
        '#title' => t('Animals'),
        '#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
      );
    }
    return $view;
  }

  /**
   * Loads all animals by name which belong to a specific feedingtrial.
   *
   * @param array $animal_names
   *   An array with animal names.
   * @param CatriaFeedingtrial $feedingtrial
   *   An object of type CatriaFeedingtrial.
   *
   * @return array
   *   Array with objects of type CatriaAnimal.
   */
  public function loadAnimalByNames(array $animal_names, CatriaFeedingtrial $feedingtrial) {
    $query = new EntityFieldQuery();
    $query
      ->entityCondition('entity_type', 'catria_animal')
      ->propertyCondition('name', $animal_names)
      ->propertyCondition('feedingtrialid', $feedingtrial->getId());
    $result = $query->execute();
    if (count($result) > 0) {
      return ($this->load(array_keys($result['catria_animal'])));
    }
    return FALSE;
  }

  /**
   * Loads specific animal by name which belongs to a specific feedingtrial.
   *
   * @param string $animal_name
   *   An animal name.
   * @param CatriaFeedingtrial $feedingtrial
   *   An object of type CatriaFeedingtrial.
   *
   * @return CatriaAnimal
   *   An objects of type CatriaAnimal.
   */
  public function loadAnimalByName($animal_name, CatriaFeedingtrial $feedingtrial) {
    $query = new EntityFieldQuery();
    $query
      ->entityCondition('entity_type', 'catria_animal')
      ->propertyCondition('name', $animal_name)
      ->propertyCondition('feedingtrialid', $feedingtrial->getId());
    $result = $query->execute();
    if (count($result) > 0) {
      $animalid = array_keys($result['catria_animal'])[0];
      return $this->loadSingle($animalid);
    }
    return FALSE;
  }

  /**
   * Loads all animals which belong to a specific feedingtrial.
   *
   * @param CatriaFeedingtrial $feedingtrial
   *   An object of type CatriaFeedingtrial.
   *
   * @return array
   *   An objects of type CatriaAnimal.
   */
  public function loadAnimalsByFeedingtrial(CatriaFeedingtrial $feedingtrial) {
    $query = new EntityFieldQuery();
    $query
      ->entityCondition('entity_type', 'catria_animal')
      ->propertyCondition('feedingtrialid', $feedingtrial->getId());
    $result = $query->execute();
    if (count($result) > 0) {
      return $this->load(array_keys($result['catria_animal']));
    }
    return FALSE;
  }

  /**
   * Delete all animals which belong to a specific feedingtrial.
   *
   * @param CatriaFeedingtrial $feedingtrial
   *   An object of type CatriaFeedingtrial.
   *
   * @return bool
   *   Boolean value.
   */
  public function deleteAnimalsByFeedingtrial(CatriaFeedingtrial $feedingtrial) {
    $animals = $this->loadAnimalsByFeedingtrial($feedingtrial);
    if ($animals) {
      $describes_controller = new CatriaDescribesController('catria_describes');
      foreach ($animals as $animal) {
        $describes_controller->deleteDescribessByAnimal($animal);
        $animal->delete();
      }
    }
    return TRUE;
  }

}
