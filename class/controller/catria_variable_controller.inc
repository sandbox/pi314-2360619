<?php

/**
 * @file
 * File contains the CatriaVariableController class.
 */

/**
 * A controller class for Catria variables.
 */
class CatriaVariableController extends CatriaEntityController {

  /**
   * Gives an overview of all variables which belong to a specific feedingtrial.
   *
   * @param CatriaFeedingtrial $feedingtrial
   *   An object of type CatriaFeedingtrial.
   *
   * @return array
   *   View object.
   */
  public function overviewFeedingtrial(CatriaFeedingtrial $feedingtrial) {
    $header = array('Name', 'Description');
    $view = array();
    $rows = array();
    if ($variables = $this->loadVariablesByFeedingtrial($feedingtrial)) {
      foreach ($variables as $variable) {
        $rows[] = array(
          l($variable->name, 'catria/variable/' . $variable->id),
          $variable->description,
        );
      }
      $view[] = array(
        '#type' => 'item',
        '#title' => t('Variables'),
        '#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
      );
    }
    return $view;
  }

  /**
   * Loads all variables which belong to a specific feedingtrial.
   *
   * @param CatriaFeedingtrial $feedingtrial
   *   An object of type CatriaFeedingtrial.
   *
   * @return array
   *   An array of objects of type CatriaVariable.
   */
  public function loadVariablesByFeedingtrial(CatriaFeedingtrial $feedingtrial) {
    $query = new EntityFieldQuery();
    $query
      ->entityCondition('entity_type', 'catria_variable')
      ->propertyCondition('feedingtrialid', $feedingtrial->getId());
    $result = $query->execute();
    if (count($result) > 0) {
      return $this->load(array_keys($result['catria_variable']));
    }
    return FALSE;
  }

  /**
   * Delete all variables which belong to a specific feedingtrial.
   *
   * @param CatriaFeedingtrial $feedingtrial
   *   An object of type CatriaFeedingtrial.
   *
   * @return bool
   *   Boolean value.
   */
  public function deleteVariablesByFeedingtrial(CatriaFeedingtrial $feedingtrial) {
    $variables = $this->loadVariablesByFeedingtrial($feedingtrial);
    if ($variables) {
      foreach ($variables as $variable) {
        $variable->delete();
      }
    }
    return TRUE;
  }

}
