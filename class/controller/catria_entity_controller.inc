<?php

/**
 * @file
 * File contains the CatriaEntityController class.
 */

/**
 * A controller class for Catria entities.
 */
class CatriaEntityController extends EntityAPIController {

  /**
   * {@inheritdoc}
   */
  public function view($entities, $view_mode = 'full', $langcode = NULL, $page = NULL) {
    $view = parent::view($entities, $view_mode = 'full', $langcode = NULL, $page = NULL);
    foreach ($entities as $entity) {
      $view[$entity->entityType()][$entity->getId()] = $entity->view();
    }
    return $view;
  }

  /**
   * Return all entities.
   *
   * @param array $entity_type_names
   *   An array with names.
   *
   * @return array
   *   An array with objects of type CatriaEntities.
   */
  public function getEntities(array $entity_type_names = NULL) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', $this->entityType);
    if (!is_null($entity_type_names)) {
      $query->entityCondition('bundle', $entity_type_names, 'IN');
    }
    $result = $query->execute();
    if (count($result) != 0) {
      return $this->load(array_keys($result[$this->entityType]));
    }
    return FALSE;
  }

  /**
   * Return all entities.
   *
   * @param array $entity_names
   *   An array with entity names.
   *
   * @return array
   *   An array with objects of type CatriaEntities.
   */
  public function getEntitiesNames(array $entity_names) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', $this->entityType);
    $query->propertyCondition('name', $entity_names, 'IN');
    $result = $query->execute();
    if (count($result) > 0) {
      return $this->load(array_keys($result[$this->entityType]));
    }
    return FALSE;
  }

  /**
   * Return a single entity by an ID.
   *
   * @param int $id
   *   An int with id.
   *
   * @return object
   *   An object of type CatriaEntity.
   */
  public function loadSingle($id) {
    if ($entityids = $this->load(array($id))) {
      return array_values($entityids)[0];
    }
    return NULL;
  }

}
