<?php

/**
 * @file
 * Contains forms for Catria measurement-objects.
 */

/**
 * Form function.
 *
 * @param array $form
 *   A form array.
 * @param array $form_state
 *   A form state array.
 * @param CatriaMeasurement $measurement
 *   An object of type CatriaMeasurement.
 *
 * @return array
 *   A form array.
 */
function catria_measurement_upload_files_form(array $form, array &$form_state, CatriaMeasurement $measurement) {
  $form_state['catria_measurement'] = $measurement;
  return $measurement->getUploadFilesForm($form, $form_state);
}

/**
 * Form function.
 *
 * @param array $form
 *   A form array.
 * @param array $form_state
 *   A form state array.
 */
function catria_measurement_upload_files_form_validate(array &$form, array &$form_state) {
  $attributefile = file_load($form_state['values']['attributefile']);
  $datafile = file_load($form_state['values']['datafile']);
  $measurement = $form_state['catria_measurement'];
  $validation = $measurement->validateFiles($attributefile, $datafile);
  if (!$validation['valid']) {
    $tmp_error = '';
    foreach ($validation['errors'] as $error) {
      $tmp_error = $tmp_error . $error;
    }
    form_set_error('validation', $tmp_error);
  }
}

/**
 * Form function.
 *
 * @param array $form
 *   A form array.
 * @param array $form_state
 *   A form state array.
 */
function catria_measurement_upload_files_form_submit(array &$form, array &$form_state) {
  $attributefile = file_load($form_state['values']['attributefile']);
  $datafile = file_load($form_state['values']['datafile']);
  $measurement = $form_state['catria_measurement'];
  $measurement->integrateFiles($attributefile, $datafile);
}

/**
 * Form function.
 *
 * @param array $form
 *   A form array.
 * @param array $form_state
 *   A form state array.
 * @param CatriaMeasurement $measurement
 *   An object of type CatriaMeasurement.
 *
 * @return array
 *   A form array.
 */
function catria_measurement_manage_status_form(array $form, array &$form_state, CatriaMeasurement $measurement) {
  $form_state['catria_measurement'] = $measurement;
  return $measurement->getManageStatusForm($form, $form_state);
}

/**
 * Form function.
 *
 * @param array $form
 *   A form array.
 * @param array $form_state
 *   A form state array.
 */
function catria_measurement_manage_status_form_submit(array &$form, array &$form_state) {
  $status = $form_state['values']['status'];
  $measurement = $form_state['catria_measurement'];
  $measurement->setStatus($status);
  $measurement->save();
}

/**
 * Form function.
 *
 * @param array $form
 *   A form array.
 * @param array $form_state
 *   A form state array.
 * @param CatriaMeasurement $measurement
 *   An object of type CatriaMeasurement.
 *
 * @return array
 *   A form array.
 */
function catria_measurement_request_form(array $form, array &$form_state, CatriaMeasurement $measurement) {
  $form_state['catria_measurement'] = $measurement;
  return $measurement->getRequestForm($form, $form_state);
}

/**
 * Form function.
 *
 * @param array $form
 *   A form array.
 * @param array $form_state
 *   A form state array.
 */
function catria_measurement_request_form_submit(array &$form, array &$form_state) {
  global $user;
  $measurement = $form_state['catria_measurement'];
  $measurement->setHas($user, 'requested');
}
