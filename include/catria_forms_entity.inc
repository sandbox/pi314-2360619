<?php

/**
 * @file
 * Contains forms for Catria entity-objects.
 */

/**
 * Form function.
 *
 * @param array $form
 *   A form array.
 * @param array $form_state
 *   A form state array.
 * @param CatriaEntity $catria_entity
 *   An object of type CatriaEntity.
 *
 * @return array
 *   A form array.
 */
function catria_entity_edit_form(array $form, array &$form_state, CatriaEntity $catria_entity) {
  $form_state['catria_entity'] = $catria_entity;
  $form = $catria_entity->getEditForm($form, $form_state);
  return $form;
}

/**
 * Form function.
 *
 * @param array $form
 *   A form array.
 * @param array $form_state
 *   A form state array.
 */
function catria_entity_edit_form_submit(array &$form, array &$form_state) {
  $catria_entity = $form_state['catria_entity'];
  $catria_entity->getEditFormSubmit($form_state);
  $catria_entity->save();
  $temp_redirect = $catria_entity->defaultUri();
  $form_state['redirect'] = $temp_redirect['path'];
}


/**
 * Form function.
 *
 * @param array $form
 *   A form array.
 * @param array $form_state
 *   A form state array.
 * @param CatriaEntity $catria_entity
 *   An object of type CatriaEntity.
 *
 * @return array
 *   A form array.
 */
function catria_entity_delete_form(array $form, array &$form_state, CatriaEntity $catria_entity) {
  $form_state['catria_entity'] = $catria_entity;
  $form = $catria_entity->getDeleteForm();
  return $form;
}

/**
 * Form function.
 *
 * @param array $form
 *   A form array.
 * @param array $form_state
 *   A form state array.
 */
function catria_entity_delete_form_submit(array $form, array &$form_state) {
  $catria_entity = $form_state['catria_entity'];
  $catria_entity->delete();
  drupal_set_message(t('The entity @name has been deleted.', array('@name' => $catria_entity->name)));
  $form_state['redirect'] = $catria_entity->getOverviewPage();
}
