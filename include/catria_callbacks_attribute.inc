<?php

/**
 * @file
 * This file contains callback functions for data overview.
 */

/**
 * Callback function.
 *
 * @param int $id
 *   An integer with id.
 * @param bool $reset
 *   A boolean value.
 *
 * @return object
 *   An object of type CatriaAttribute.
 */
function catria_attribute_load($id, $reset = FALSE) {
  if (is_numeric($id)) {
    $catria_attributes = catria_attribute_load_multiple(array((int) $id), array(), $reset);
    return reset($catria_attributes);
  }
  return FALSE;
}

/**
 * Callback function.
 *
 * @param array $ids
 *   An array with ids.
 * @param array $conditions
 *   An array with conditions.
 * @param bool $reset
 *   A boolean value.
 *
 * @return array
 *   An array with objects of type CatriaAttribute.
 */
function catria_attribute_load_multiple($ids = array(), $conditions = array(), $reset = FALSE) {
  if ($reset) {
    entity_get_controller('catria_attribute')->resetCache();
  }
  return entity_get_controller('catria_attribute')->load($ids, $conditions);
}

/**
 * Callback function.
 *
 * @param string $access
 *   A string with access.
 * @param CatriaAttribute $attribute
 *   An object of type CatriaAttribute.
 *
 * @return bool
 *   A boolean value.
 */
function catria_edit_attribute_access($access, CatriaAttribute $attribute) {
  global $user;
  if ($attribute->getMeasurement()->getHasAccess($user) == 'administrator') {
    return user_access($access, $user);
  }
  return FALSE;
}
