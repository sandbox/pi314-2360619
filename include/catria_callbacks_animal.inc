<?php

/**
 * @file
 * This file contains callback functions for Catria animal-objects.
 */

/**
 * Callback function.
 *
 * @param int $id
 *   An integer with id.
 * @param bool $reset
 *   A boolean value.
 *
 * @return object
 *   An object of type CatriaMeasurement.
 */
function catria_animal_load($id, $reset = FALSE) {
  if (is_numeric($id)) {
    $catria_animals = catria_animal_load_multiple(array((int) $id), array(), $reset);
    return reset($catria_animals);
  }
  return FALSE;
}

/**
 * Callback function.
 *
 * @param array $ids
 *   An array with ids.
 * @param array $conditions
 *   An array with conditions.
 * @param bool $reset
 *   A boolean value.
 *
 * @return array
 *   An array with objects of type CatriaAnimal.
 */
function catria_animal_load_multiple($ids = array(), $conditions = array(), $reset = FALSE) {
  if ($reset) {
    entity_get_controller('catria_animal')->resetCache();
  }
  return entity_get_controller('catria_animal')->load($ids, $conditions);
}

/**
 * Callback function.
 *
 * @param string $access
 *   A string with access.
 * @param CatriaAnimal $animal
 *   An object of type CatriaAnimal.
 *
 * @return bool
 *   A boolean value.
 */
function catria_edit_animal_access($access, CatriaAnimal $animal) {
  global $user;
  if ($animal->getFeedingtrial()->userid == $user->uid) {
    return user_access($access, $user);
  }
  return FALSE;
}
