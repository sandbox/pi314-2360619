<?php

/**
 * @file
 * This file contains callback functions for data overview.
 */

/**
 * Callback function.
 *
 * @param int $id
 *   An integer with id.
 * @param bool $reset
 *   A boolean value.
 *
 * @return object
 *   An object of type CatriaVariable.
 */
function catria_variable_load($id, $reset = FALSE) {
  if (is_numeric($id)) {
    $catria_variables = catria_variable_load_multiple(array((int) $id), array(), $reset);
    return reset($catria_variables);
  }
  return FALSE;
}

/**
 * Callback function.
 *
 * @param array $ids
 *   An array with ids.
 * @param array $conditions
 *   An array with conditions.
 * @param bool $reset
 *   A boolean value.
 *
 * @return array
 *   An array with objects of type CatriaVariable.
 */
function catria_variable_load_multiple($ids = array(), $conditions = array(), $reset = FALSE) {
  if ($reset) {
    entity_get_controller('catria_variable')->resetCache();
  }
  return entity_get_controller('catria_variable')->load($ids, $conditions);
}

/**
 * Callback function.
 *
 * @param string $access
 *   A string with access.
 * @param CatriaVariable $variable
 *   An object of type CatriaVariable.
 *
 * @return array
 *   A form array.
 */
function catria_edit_variable_access($access, CatriaVariable $variable) {
  global $user;
  if ($variable->getFeedingtrial()->userid == $user->uid) {
    return user_access($access, $user);
  }
  return FALSE;
}
