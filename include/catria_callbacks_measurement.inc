<?php

/**
 * @file
 * This file contains callback functions for measurement-objects.
 */

/**
 * Callback function.
 *
 * @param int $id
 *   An integer with id.
 * @param bool $reset
 *   A boolean value.
 *
 * @return object
 *   An object of type CatriaMeasurement.
 */
function catria_measurement_load($id, $reset = FALSE) {
  if (is_numeric($id)) {
    $catria_measurements = catria_measurement_load_multiple(array((int) $id), array(), $reset);
    return reset($catria_measurements);
  }
  return FALSE;
}

/**
 * Callback function.
 *
 * @param array $ids
 *   An array with ids.
 * @param array $conditions
 *   An array with conditions.
 * @param bool $reset
 *   A boolean value.
 *
 * @return array
 *   An array with objects of type CatriaMeasurement.
 */
function catria_measurement_load_multiple(array $ids = array(), array $conditions = array(), $reset = FALSE) {
  if ($reset) {
    entity_get_controller('catria_measurement')->resetCache();
  }
  return entity_get_controller('catria_measurement')->load($ids, $conditions);
}

/**
 * Callback function.
 *
 * @param CatriaMeasurement $measurement
 *   An object of type CatriaMeasurement.
 *
 * @return array
 *   A form array.
 */
function catria_measurement_manage_status(CatriaMeasurement $measurement) {
  return drupal_get_form('catria_measurement_manage_status_form', $measurement);
}

/**
 * Callback function.
 *
 * @param CatriaMeasurement $measurement
 *   An object of type CatriaMeasurement.
 * @param object $user
 *   An object of type User.
 *
 * @return array
 *   A form array.
 */
function catria_measurement_set_allow_user(CatriaMeasurement $measurement, $user) {
  $measurement->setHas($user, 'allowed');
  return drupal_get_form('catria_measurement_manage_status_form', $measurement);
}

/**
 * Callback function.
 *
 * @param CatriaMeasurement $measurement
 *   An object of type CatriaMeasurement.
 * @param object $user
 *   An object of type User.
 *
 * @return array
 *   A form array.
 */
function catria_measurement_set_refuse_user(CatriaMeasurement $measurement, $user) {
  $measurement->removeHas($user);
  return drupal_get_form('catria_measurement_manage_status_form', $measurement);
}

/**
 * Callback function.
 *
 * @param CatriaEntity $catria_entity
 *   An object of type CatriaEntity.
 *
 * @return array
 *   A form array.
 */
function catria_measurement_request(CatriaEntity $catria_entity) {
  return drupal_get_form('catria_measurement_request_form', $catria_entity);
}

/**
 * Callback function.
 *
 * @param string $access
 *   A string with access.
 * @param CatriaMeasurement $measurement
 *   An object of type CatriaMeasurement.
 *
 * @return bool
 *   A boolean value.
 */
function catria_edit_measurement_access($access, CatriaMeasurement $measurement) {
  global $user;
  if ($measurement->getHasAccess($user) == 'administrator') {
    return user_access($access, $user);
  }
  return FALSE;
}

/**
 * Callback function.
 *
 * @param string $access
 *   A string with access.
 * @param CatriaMeasurement $measurement
 *   An object of type CatriaMeasurement.
 *
 * @return bool
 *   A boolean value.
 */
function catria_measurement_request_access($access, CatriaMeasurement $measurement) {
  if ($measurement->getStatus() == 'restricted') {
    return user_access($access);
  }
  else {
    return FALSE;
  }
}
