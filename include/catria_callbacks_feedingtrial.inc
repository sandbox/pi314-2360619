<?php

/**
 * @file
 * Contains callback function for feedingtrial-objects.
 */

/**
 * Callback function.
 *
 * @param int $id
 *   An integer with id.
 * @param bool $reset
 *   A boolean value.
 *
 * @return object
 *   An object of type CatriaFeedingtrial.
 */
function catria_feedingtrial_load($id, $reset = FALSE) {
  if (is_numeric($id)) {
    $catria_feedingtrials = catria_feedingtrial_load_multiple(array((int) $id), array(), $reset);
    return reset($catria_feedingtrials);
  }
  return FALSE;
}

/**
 * Callback function.
 *
 * @param array $ids
 *   An array with ids.
 * @param array $conditions
 *   An array with conditions.
 * @param bool $reset
 *   A boolean value.
 *
 * @return array
 *   An array with objects of type CatriaFeedingtrial.
 */
function catria_feedingtrial_load_multiple($ids = array(), $conditions = array(), $reset = FALSE) {
  if ($reset) {
    entity_get_controller('catria_feedingtrial')->resetCache();
  }
  return entity_get_controller('catria_feedingtrial')->load($ids, $conditions);
}

/**
 * Callback function.
 *
 * @return array
 *   A form array.
 */
function catria_feedingtrial_overview() {
  return entity_get_controller('catria_feedingtrial')->overview();
}

/**
 * Callback function.
 *
 * @return array
 *   A form array.
 */
function catria_myfeedingtrial_overview() {
  return entity_get_controller('catria_feedingtrial')->overviewUser();
}

/**
 * Callback function.
 *
 * @param CatriaFeedingtrial $feedingtrial
 *   An object of type CatriaFeedingtrial.
 *
 * @return array
 *   A form array.
 */
function catria_feedingtrial_statistics_view(CatriaFeedingtrial $feedingtrial) {
  drupal_set_title(t('Statistics for feedingtrial') . ' ' . $feedingtrial->name);
  $content = $feedingtrial->statisticsView();
  return $content;
}

/**
 * Callback function.
 *
 * @param string $access
 *   A string with access.
 * @param CatriaFeedingtrial $feedingtrial
 *   An object of type CatriaFeedingtrial.
 *
 * @return bool
 *   A boolean value.
 */
function catria_edit_feedingtrial($access, CatriaFeedingtrial $feedingtrial) {
  global $user;
  if (user_access($access, $user)) {
    return $feedingtrial->userid == $user->uid;
  }
  return FALSE;
}
