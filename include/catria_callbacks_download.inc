<?php

/**
 * @file
 * This file contains callback functions for data overview.
 */

/**
 * Callback function.
 *
 * @param CatriaFeedingtrial $feedingtrial
 *   An object of type CatriaFeedingtrial.
 *
 * @return array
 *   A form array.
 */
function catria_download(CatriaFeedingtrial $feedingtrial) {
  drupal_set_title(t('Download data') . ': ' . $feedingtrial->getName());
  return drupal_get_form('catria_download_form', $feedingtrial);
}
