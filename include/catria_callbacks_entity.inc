<?php

/**
 * @file
 * This file contains callback functions for entity-objects.
 */

/**
 * Callback function.
 *
 * @param string $entity_type
 *   A string with entity type.
 *
 * @return array
 *   A form array.
 */
function catria_entity_add($entity_type) {
  $catria_entity = catria_entity_create($entity_type);
  drupal_set_title($catria_entity::ADDTITLE);
  return drupal_get_form($catria_entity->getEditFormCallback(), $catria_entity);
}

/**
 * Callback function.
 *
 * @param string $entity_type
 *   A string with entity type.
 * @param array $values
 *   An array with values.
 *
 * @return object
 *   An object of type entity_type.
 */
function catria_entity_create($entity_type, $values = array()) {
  return entity_get_controller($entity_type)->create($values);
}

/**
 * Callback function.
 *
 * @param CatriaEntity $catria_entity
 *   An object of type CatriaEntity.
 *
 * @return string
 *   A string with name.
 */
function catria_entity_page_title(CatriaEntity $catria_entity) {
  return $catria_entity->getName();
}

/**
 * Callback function.
 *
 * @param CatriaEntity $catria_entity
 *   An object of type CatriaEntity.
 * @param string $view_mode
 *   A string with view mode.
 *
 * @return array
 *   A form array.
 */
function catria_entity_page_view(CatriaEntity $catria_entity, $view_mode = 'full') {
  drupal_set_title($catria_entity->name);
  $content = $catria_entity->view();
  return $content;
}

/**
 * Callback function.
 *
 * @param CatriaEntity $catria_entity
 *   An object of type CatriaEntity.
 *
 * @return array
 *   A form array.
 */
function catria_entity_page_edit(CatriaEntity $catria_entity) {
  return drupal_get_form($catria_entity->getEditFormCallback(), $catria_entity);
}

/**
 * Callback function.
 *
 * @param CatriaEntity $catria_entity
 *   An object of type CatriaEntity.
 *
 * @return array
 *   A form array.
 */
function catria_entity_page_delete(CatriaEntity $catria_entity) {
  return drupal_get_form('catria_entity_delete_form', $catria_entity);
}
