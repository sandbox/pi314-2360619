<?php

/**
 * @file
 * Contains forms for Catria feedingtrial-objects.
 */

/**
 * Form function.
 *
 * @param array $form
 *   A form array.
 * @param array $form_state
 *   A form state array.
 * @param CatriaFeedingtrial $feedingtrial
 *   An object of type CatriaEntity.
 *
 * @return array
 *   A form array.
 */
function catria_feedingtrial_upload_variablefile_form(array $form, array &$form_state, CatriaFeedingtrial $feedingtrial) {
  $form_state['catria_feedingtrial'] = $feedingtrial;
  return $feedingtrial->getUploadVariablefileForm($form, $form_state);
}

/**
 * Form function.
 *
 * @param array $form
 *   A form array.
 * @param array $form_state
 *   A form state array.
 */
function catria_feedingtrial_upload_variablefile_form_validate(array &$form, array &$form_state) {
  $file = file_load($form_state['values']['file']);
  $feedingtrial = $form_state['catria_feedingtrial'];
  if (!$feedingtrial->validateVariablefile($file)) {
    form_error($form, t('File is not valid.'));
  }
}

/**
 * Form function.
 *
 * @param array $form
 *   A form array.
 * @param array $form_state
 *   A form state array.
 */
function catria_feedingtrial_upload_variablefile_form_submit(array &$form, array &$form_state) {
  $file = file_load($form_state['values']['file']);
  $feedingtrial = $form_state['catria_feedingtrial'];
  $feedingtrial->setVariablefile($file);
  $temp_redirect = $feedingtrial->defaultUri();
  $form_state['redirect'] = $temp_redirect['path'];
}

/**
 * Form function.
 *
 * @param array $form
 *   A form array.
 * @param array $form_state
 *   A form state array.
 * @param CatriaFeedingtrial $feedingtrial
 *   An object of type CatriaEntity.
 *
 * @return array
 *   A form array.
 */
function catria_feedingtrial_delete_variables_form(array $form, array &$form_state, CatriaFeedingtrial $feedingtrial) {
  $form_state['catria_feedingtrial'] = $feedingtrial;
  return $feedingtrial->getDeleteVariablesForm($form, $form_state);
}

/**
 * Form function.
 *
 * @param array $form
 *   A form array.
 * @param array $form_state
 *   A form state array.
 *
 * @return array
 *   A form array.
 */
function catria_feedingtrial_delete_variables_form_submit(array &$form, array &$form_state) {
  $feedingtrial = $form_state['catria_feedingtrial'];
  $feedingtrial->deleteVariables();
  $temp_redirect = $feedingtrial->defaultUri();
  $form_state['redirect'] = $temp_redirect['path'];
  return TRUE;
}

/**
 * Form function.
 *
 * @param array $form
 *   A form array.
 * @param array $form_state
 *   A form state array.
 * @param CatriaFeedingtrial $feedingtrial
 *   An object of type CatriaEntity.
 *
 * @return array
 *   A form array.
 */
function catria_feedingtrial_upload_animalfile_form(array $form, array &$form_state, CatriaFeedingtrial $feedingtrial) {
  $form_state['catria_feedingtrial'] = $feedingtrial;
  return $feedingtrial->getUploadAnimalfileForm($form, $form_state);
}

/**
 * Form function.
 *
 * @param array $form
 *   A form array.
 * @param array $form_state
 *   A form state array.
 */
function catria_feedingtrial_upload_animalfile_form_validate(array &$form, array &$form_state) {
  $file = file_load($form_state['values']['animalfile']);
  $feedingtrial = $form_state['catria_feedingtrial'];
  if (!$feedingtrial->validateAnimalfile($file)) {
    form_error($form, t('File is not valid.'));
  }
}

/**
 * Form function.
 *
 * @param array $form
 *   A form array.
 * @param array $form_state
 *   A form state array.
 */
function catria_feedingtrial_upload_animalfile_form_submit(array &$form, array &$form_state) {
  $file = file_load($form_state['values']['animalfile']);
  $feedingtrial = $form_state['catria_feedingtrial'];
  $feedingtrial->setAnimalfile($file);
  $temp_redirect = $feedingtrial->defaultUri();
  $form_state['redirect'] = $temp_redirect['path'];
}

/**
 * Form function.
 *
 * @param array $form
 *   A form array.
 * @param array $form_state
 *   A form state array.
 * @param CatriaFeedingtrial $feedingtrial
 *   An object of type CatriaEntity.
 *
 * @return array
 *   A form array.
 */
function catria_feedingtrial_delete_animals_form(array $form, array &$form_state, CatriaFeedingtrial $feedingtrial) {
  $form_state['catria_feedingtrial'] = $feedingtrial;
  return $feedingtrial->getDeleteAnimalsForm($form, $form_state);
}

/**
 * Form function.
 *
 * @param array $form
 *   A form array.
 * @param array $form_state
 *   A form state array.
 *
 * @return array
 *   A form array.
 */
function catria_feedingtrial_delete_animals_form_submit(array &$form, array &$form_state) {
  $feedingtrial = $form_state['catria_feedingtrial'];
  $feedingtrial->deleteAnimals();
  $temp_redirect = $feedingtrial->defaultUri();
  $form_state['redirect'] = $temp_redirect['path'];
  return TRUE;
}
