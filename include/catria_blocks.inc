<?php

/**
 * @file
 * This file contains Catria blocks.
 */

/**
 * Returns block.
 *
 * @return array
 *   An array with block.
 */
function catria_ui_overview() {
  global $user;

  $links = array();
  $links_l1 = array();
  $links_l2 = array();
  $links_l3 = array();

  if (isset(menu_get_item(current_path())['map'])) {
    $currentpath = menu_get_item(current_path())['map'];
  }
  else {
    $currentpath = array();
  }
  $links['Feedingtrials'] = array(
    'data' => l(t('Feedingtrials'), 'catria/feedingtrials'),
    'children' => array(),
  );
  if ($user->uid) {
    $links['MyData'] = array(
      'data' => l(t('My data'), 'catria/mydata'),
      'children' => array(),
    );
    $links['Account'] = array(
      'data' => l(t('My account'), 'user'),
      'children' => array(),
    );
    $links['Logout'] = array(
      'data' => l(t('Log out'), 'user/logout'),
      'children' => array(),
    );
  }
  if (isset($currentpath[1])) {
    if ($currentpath[0] == 'catria') {
      if ($currentpath[1] == 'mydata') {
        $links['MyData']['children'][] = array(
          l(t('Add feedingtrial'), 'catria/mydata/add_feedingtrial/'),
        );
        $links['MyData']['children'][] = array(
          l(t('Add measurement'), 'catria/mydata/add_measurement/'),
        );
      }
      if ($currentpath[1] == 'feedingtrials' || isset($currentpath[2])) {
        if ($feedingtrials = entity_get_controller('catria_feedingtrial')->getEntities()) {
          foreach ($feedingtrials as $feedingtrial) {
            $links_l1[$feedingtrial->getid()] = l($feedingtrial->getName(), 'catria/feedingtrial/' . $feedingtrial->getid());
          }
        }
        if (isset($currentpath[2])) {
          if (is_object($currentpath[2])) {
            if ($currentpath[1] == 'feedingtrial') {
              if ($feedingtrials = entity_get_controller('catria_feedingtrial')->getEntities()) {
                foreach ($feedingtrials as $feedingtrial) {
                  $links_l1[$feedingtrial->getid()] = l($feedingtrial->getName(), 'catria/feedingtrial/' . $feedingtrial->getid());
                }
              }
            }
            if ($currentpath[1] == 'variable') {
              $variable = $currentpath[2];
              $feedingtrial = $variable->getFeedingtrial();
              $links_l2 = array(l($variable->getName(), 'catria/variable/' . $variable->getid()));
              $links_l1[$feedingtrial->getid()] = array(
                'data' => $links_l1[$feedingtrial->getid()],
                'children' => $links_l2,
              );
            }
            if ($currentpath[1] == 'animal') {
              $animal = $currentpath[2];
              $feedingtrial = $animal->getFeedingtrial();
              $links_l2 = array(l($animal->getName(), 'catria/animal/' . $animal->getid()));
              $links_l1[$feedingtrial->getid()] = array(
                'data' => $links_l1[$feedingtrial->getid()],
                'children' => $links_l2,
              );
            }

            if ($currentpath[1] == 'measurement') {
              $measurement = $currentpath[2];
              $feedingtrial = $measurement->getFeedingtrial();
              $links_l2 = array(l($measurement->getName(), 'catria/measurement/' . $measurement->getid()));
              $links_l1[$measurement->getFeedingtrial()->getid()] = array(
                'data' => $links_l1[$feedingtrial->getid()],
                'children' => $links_l2,
              );
            }
            if ($currentpath[1] == 'attribute') {
              $attribute = $currentpath[2];
              $measurement = $attribute->getMeasurement();
              $feedingtrial = $measurement->getFeedingtrial();
              $links_l3 = array(l($attribute->getName(), 'catria/attribute/' . $attribute->getid()));
              $links_l2[$measurement->getid()] = array(
                'data' => l($measurement->getName(), 'catria/measurement/' . $measurement->getid()),
                'children' => $links_l3,
              );
              $links_l1[$feedingtrial->getid()] = array(
                'data' => $links_l1[$feedingtrial->getid()],
                'children' => $links_l2,
              );
            }
          }
        }
        $links['Feedingtrials']['children'] = $links_l1;
      }
    }
  }
  $content = theme('item_list', array('items' => $links));
  $block = array(
    'subject' => t('Catria Database'),
    'content' => $content,
  );
  return $block;
}
