<?php

/**
 * @file
 * Contains forms for Catria download-objects.
 */

/**
 * Form function.
 *
 * @param array $form
 *   A form array.
 * @param array $form_state
 *   A form state array.
 * @param CatriaFeedingtrial $feedingtrial
 *   An object of type CatriaEntity.
 *
 * @return array
 *   A form array.
 */
function catria_download_form(array $form, array &$form_state, CatriaFeedingtrial $feedingtrial) {
  $download = new CatriaDownload();
  $form_state['catria_feedingtrial'] = $feedingtrial;
  $form_state['catria_download'] = $download;
  return $download->getDownloadForm($form, $form_state);
}

/**
 * Form function.
 *
 * @param array $form
 *   A form array.
 * @param array $form_state
 *   A form state array.
 */
function catria_download_form_submit(array &$form, array &$form_state) {
  $download = $form_state['catria_download'];
  $download->getDownloadFormSubmit($form_state);
}
