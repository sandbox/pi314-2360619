CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
 * Catria (collaborative animal trials) provides tools for the management of
   data from feeding trials. Users can integrate, share and download data-
   sets. Scientific projects can use Catria to implement a project-database 
   for the management and publication of their measurement data from feeding 
   trials.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/pi314/2360619

REQUIREMENTS
------------
This module requires the following modules:
 * Entity API (https://www.drupal.org/project/entity)

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * There is no configuration required.

 * Add a customized user-menu at Administration >> Structure >> Blocks.


MAINTAINERS
-----------
Current maintainers:
 * Sven Twardziok - https://www.drupal.org/user/3073175/
